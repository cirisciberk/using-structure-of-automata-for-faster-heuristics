#!/usr/bin/python
import os
import sys

cmdTemplate = "./heuristics %d %d %d"

def tester(stateSizes, alphabetSizes, experimentSize):
    for s in stateSizes:
        for p in alphabetSizes:
            print "stateSize :", s, "alphabetSize :", p
            sys.stdout.write("00%")
            for expID in range(experimentSize):
                cmd = cmdTemplate % (s, p, expID)
                retval = os.system(cmd + " > /dev/null")
                if retval != 0:
                    exit(1)
                    
                percentage = (expID + 1) / (experimentSize / 100.0)
                sys.stdout.write("\r%02d" % percentage)
                sys.stdout.write("%")
                sys.stdout.flush()
            print


def main():
    os.system("make clean")
    os.system("make")
    stateSizes = [50, 100, 200, 500, 1000]
    alphabetSizes = [10, 2]
    experimentSize = 20
    tester(stateSizes, alphabetSizes, experimentSize)
 

if __name__ == "__main__":
    main()
