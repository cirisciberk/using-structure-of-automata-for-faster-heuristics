#include "graph.h"

int main(int argc, char *argv[]) {
	if (argc >= 6) {
		FILE *file = fopen(argv[1], "w+");
		FILE *pair_file = fopen(argv[2], "w+");

		int i, inputs = atoi(argv[4]), rand_seed = atoi(argv[3]);
		srand(rand_seed);
		Graph **gArr = (Graph **) malloc(sizeof(Graph *) * (argc - 5));
		for (i = 5; i < argc; i++) {
			long long start = current_timestamp();
			Graph *graph = generateGraph(atoi(argv[i]), inputs);
			fillGraph(graph, rand_seed & 1); long long finish = current_timestamp(); 
			saveGraph(graph, file);
			printf("Graph %d with %d states, %d inputs and %d SCC(s) in %lld ms\n", i - 4, graph->states, graph->inputs, graph->SCCs, finish - start);
			gArr[i - 5] = graph;
		}
		if (argc - 5> 1) {
			long long start = current_timestamp();
			Graph *graph = connectGraphs(gArr, argc - 5); long long finish = current_timestamp();
			printf("Connected %d graphs: Graph with %d states, %d inputs and %d SCC(s) in %lld ms\n", argc - 5, graph->states, graph->inputs, graph->SCCs, finish - start);
			saveGraph(graph, file);
			destroyGraph(graph); free(graph);
		}
		
		for (i = 0; i < argc - 5; i++) {
			
			destroyGraph(gArr[i]); free(gArr[i]);
			
		}
		fclose(file);
		file = fopen(argv[1], "r");
		
		Graph *lastGraph = 0;
		Graph *graph = 0;
		while((graph = loadGraph(file))) 
		{
			if (lastGraph) { destroyGraph(lastGraph); free(lastGraph); }
			lastGraph = graph;

		} 
		graph = lastGraph;


		Graph *pairGraph = generatePairGraph(graph);

	
		printf("Generated pair graph\n");


		saveGraph(pairGraph, pair_file);
		destroyGraph(graph); destroyGraph(pairGraph); free(graph); free(pairGraph);

		free(gArr);
		fclose(file);
		fclose(pair_file);
		printf("Check out the file: %s\n", argv[1]);
		printf("Check out the file for pair automata: %s\n", argv[2]);
	} else {
		printf("usage: %s filename  pair_filename rand_seed inputs states+\n", argv[0]);
		return 1;
	}
	return 0;
}