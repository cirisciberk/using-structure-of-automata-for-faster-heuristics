#include "graph.h"

/*int main(int argc, char *argv[]) {
	if (argc >= 5) {
		FILE *file = fopen(argv[1], "w+");
		int i, inputs = atoi(argv[3]), rand_seed = atoi(argv[2]);
		srand(rand_seed);
		Graph **gArr = (Graph **) malloc(sizeof(Graph *) * (argc - 4));
		for (i = 4; i < argc; i++) {
			long long start = current_timestamp();
			Graph *graph = generateGraph(atoi(argv[i]), inputs);
			fillGraph(graph, rand_seed & 1); long long finish = current_timestamp(); 
			saveGraph(graph, file);
			printf("Graph %d with %d states, %d inputs and %d SCC(s) in %lld ms\n", i - 4, graph->states, graph->inputs, graph->SCCs, finish - start);
			gArr[i - 4] = graph;
		}
		if (argc - 4 > 1) {
			long long start = current_timestamp();
			Graph *graph = connectGraphs(gArr, argc - 4); long long finish = current_timestamp();
			printf("Connected %d graphs: Graph with %d states, %d inputs and %d SCC(s) in %lld ms\n", argc - 4, graph->states, graph->inputs, graph->SCCs, finish - start);
			saveGraph(graph, file);
			destroyGraph(graph); free(graph);
		}
		for (i = 0; i < argc - 4; i++) {
			destroyGraph(gArr[i]); free(gArr[i]);
		}
		free(gArr);
		fclose(file);
		printf("Check out the file: %s\n", argv[1]);
	} else {
		printf("usage: %s filename rand_seed inputs states+\n", argv[0]);
		return 1;
	}
	return 0;
}*/

void printAutomata(int* automata, int N, int p) {
	printf("Automata ----------------------\n");
  int i, j;
	for (i = 0; i < p; ++i) {
		printf("letter %c:\t", (char)(i + 97));
		for (j = 0; j < N; ++j) {
			printf("%d\t" , automata[i * N + j]);
		}
		printf("\n");
	}
}

int main(int argc, char *argv[])
{
  //printf("%d\n", argc);
	if (argc >= 6) {
		FILE *file = fopen(argv[1], "w+");
		int i, inputs = atoi(argv[3]), rand_seed = atoi(argv[2]);
		srand(rand_seed);
		Graph **gArr = (Graph **) malloc(sizeof(Graph *) * atoi(argv[4]));
    	Graph * graph;
		for (i = 5; i < atoi(argv[4]) + 5; i++) {
			long long start = current_timestamp();
			graph = generateGraph(atoi(argv[i]), inputs);
			fillGraph(graph, rand_seed & 1); long long finish = current_timestamp();
			/*
			int N = graph->states, P = graph->inputs;
      		int * automata = graph->data;
      		printAutomata(automata, N, P);
      		*/
			saveGraph(graph, file);
			gArr[i - 5] = graph;
			if(i == argc-1 && argc == 6)  // Since there is one SCC, no connectGraphs function call.
			{
				int N = graph->states, P = graph->inputs;
				int * automata = graph->data;
				printAutomata(automata, N, P);
        		saveGraph(graph, file);
			}
		}
		if (argc - 6 >= 1) {
			int j, totalState = 0;
			long long start = current_timestamp();
			for(j = 5; j < 5+atoi(argv[4]); j++)
			{
  				totalState += atoi(argv[j]);
			}

  			Graph * last = generateGraph(totalState, atoi(argv[3]));
			ControlledSCCconnector(gArr, argv, argc, last);
			//printf("Graph SCC Number after elemination: %d\n", last->SCCs);
			/*
			int N = last->states, P = last->inputs;
			int * automata = last->data;
			printAutomata(automata, N, P);*/
			saveGraph(last, file);
			fclose(file);

		/*	
		printf("Graph SCC Number before elemination: %d\n", graph->SCCs);
			printf("Graph :\n");
			int N = graph->states, P = graph->inputs;
			int * automata = graph->data;
			printAutomata(automata, N, P);
			ControlledSCCconnector(graph);  //Bunu d�zelt
			automata = graph->data;
			printf("Graph SCC Number after elemination: %d\n", graph->SCCs);
			printf("Graph :\n");
			printAutomata(automata, N, P);*/
			//destroyGraph(graph); free(graph);
		}

		//Sonuncu edge'i elemeyi d�zelt.
		for (i = 0; i < atoi(argv[4]); i++) {
			destroyGraph(gArr[i]); free(gArr[i]);
		}
		free(gArr);
		printf("Check out the file: %s\n", argv[1]);
		
	}
	else {
		printf("usage: %s filename rand_seed inputs #ofSCC states+ (from_scc perc. to_scc)+ \n", argv[0]);
		return 1;
	}

	return 0;
}