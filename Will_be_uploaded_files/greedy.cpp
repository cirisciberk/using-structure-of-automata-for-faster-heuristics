#include <iostream>
#include <fstream>
#include <cmath>
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
//#include "omp.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <queue> 
#include "graph.h"

#define TIMER
//#define DEBUG
#ifdef TIMER
#define TIME_ANALYSIS 1 //#define TIMER macro is required for time analysis
#endif
#define DATA_ANALYSIS 2
#define LOG_LEVEL TIME_ANALYSIS
using namespace std;

#ifdef LOG_LEVEL
ofstream out;
#endif

struct PNode {
	int letter;
	PNode* next;
	PNode(int _letter, PNode* _next) : letter(_letter), next(_next) {}
};

void insertToPath(int letter, PNode* &head, PNode* &last) {//inserts the element at the back of the path, if empty path becomes element
	PNode* temp = new PNode(letter, NULL);
	if (head == NULL) {
		head = last = temp;
	} else {
		last = last->next = temp;
	}
}

void printAutomata(int* automata, int N, int p) {
	cout << "Automata ----------------------" << endl;
	for (int i = 0; i < p; ++i) {
		cout << "letter " << (char)(i + 97) << ":\t";
		for (int j = 0; j < N; ++j) {
			cout << automata[i * N + j] << "\t";
		}
		cout << endl;
	}
}

void printInverseAutomata(int* inv_automata_ptrs, int* inv_automata, int N, int p) {
	cout << "Inverse Automata --------------" << endl;
	for (int i = 0; i < p; ++i) {
		cout << "letter " << (char)(i + 97) << ":\n";
		/* for (int j = 0; j <= N; ++j) {
			cout << inv_automata_ptrs[i * (N + 1) + j] << "\t";
		} */
		cout << endl;
		for (int j = 0; j < N; ++j) {
			cout << inv_automata[i * N + j] << "\t";
		}
		cout << endl << endl;
	}
}

#define Id(s1, s2) ((s1 > s2)?(((s1 * (s1 + 1))/2) + s2):(((s2 * (s2 + 1))/2) + s1)) //this is how we compute the ids(pair id)
#define s1fromId(id) ((int)(sqrt((2.0 * id) +1.0) - 0.5));//get s1 id from pair id, there is square root because when computing pair id we square the element.
#define s2fromId(id, s1) (id - ((s1 * (s1 + 1))/2));//given id and s1, returns s2

//a is automata a[i][j] -> state j goes to a[i][j] with letter j
//iap is inverse automata pointers -> ia[i][iap[i][j] ... ia[i][j+1]] keeps the state ids which go to state j with letter i
//there are N states and p letters in the automata
void greedyHeuristic_naive(int* a, int* iap, int* ia, int N, int P, PNode* &path, PNode* &globpath, int* actives, int no_actives) {
	int noOfPair = (N * (N + 1)) / 2;
	int* active_marker = new int[N];    // active kısmı dışardan alcak şekilde düzenle

	int* distance = new int[noOfPair];
	int* next = new int[noOfPair];
	int* letter = new int[noOfPair];
	int* que = new int[noOfPair];

	

/*#ifdef TIMER
	double t1 = omp_get_wtime();
	double total = 0;
#endif*/
#if LOG_LEVEL == DATA_ANALYSIS
	int max_of_min_distances = 0;
	int min_distance_counter = 0;
	int min_distance_sum = 0;
#endif

	for (int i = 0; i < noOfPair; i++) {
		distance[i] =  -1;//initialize as -1
	}

	//BFS queue for the pairs. Bfs stands for breadth first search. The bfs needs to store back-pointers to predeseccor in each state hence the inverse automata pointers.
	int qs = 0;
	int qe = 0;
	int starpid;

	for (int i = 0; i < N; ++i) {//start from 1 to N
		int id = Id(i, i);//(((i * (i + 1))/2) + i) = (i^2 + 3i)/2 
		distance[id] = 0;
		que[qe++] = id;//put id's in que queue starting from index 0 until index n-1
	}

	//there are more nodes in the queue
	while (qs < qe && qe < noOfPair) {
		int q_id = que[qs++];
		int q_dist = distance[q_id];

		//will process the pair with id q_id now
		int q_s1 = s1fromId(q_id); //the first state in the pair
		int q_s2 = s2fromId(q_id, q_s1); //the second state in the pair (we are sure that q_s1 >= q_s2)

#ifdef DEBUG
		cout << "will process " << q_s1 << " " << q_s2 << " with id  " << q_id << " with distance " << q_dist << endl;
#endif


		for (int p = 0; p < P; p++) {
			int* p_ia = &ia[p * N]; //this is the inverse automata for letter p
			int* p_iap = &iap[p * (N + 1)]; //and its state pointers
			int iap_s1_limit = p_iap[q_s1 + 1];
			int iap_s2_limit = p_iap[q_s2 + 1];

			if (p_iap[q_s2] == iap_s2_limit) continue;

			for (int iap_s1_ptr = p_iap[q_s1]; iap_s1_ptr < iap_s1_limit; ++iap_s1_ptr) {
				int ia_s1 = p_ia[iap_s1_ptr];
				for (int iap_s2_ptr = p_iap[q_s2]; iap_s2_ptr < iap_s2_limit; ++iap_s2_ptr) {
					int ia_s2 = p_ia[iap_s2_ptr];
					int ia_id = Id(ia_s1, ia_s2);


					if (distance[ia_id] < 0) { //we found an unvisited pair. so we need to add this to the queue
						distance[ia_id] = q_dist + 1;

						next[ia_id] = q_id;
						letter[ia_id] = p;
						que[qe++] = ia_id;
					}
				}
			}
		}
	}
	
/*#ifdef TIMER
	double time = omp_get_wtime() - t1;
	total += time;
	cout << "BFS tree generation takes " << time << " seconds\n";
#if LOG_LEVEL == TIME_ANALYSIS
	out << total << ",";
#endif
#endif*/

/*#ifdef TIMER
	t1 = omp_get_wtime();
#endif*/

	PNode* last = NULL;
	PNode* last2 = globpath;
	memset(active_marker, 0, sizeof(int) * N);
	
	//Inital path yolla algorithmadaki süpürünce kalan nodelar nerde gör.

	//Changes will be here, give star a very big distance
	int step = 1;
	while (no_actives > 1) {
		//find the pair id with minimum distance
		int min_distance = N * N;//Max value is given such that it will be swapped with next minimum
		int min_id;
		for (int i = 0; i < no_actives; i++) {
			for (int j = 0; j < i; j++) {
				int s1 = actives[i];
				int s2 = actives[j];

				int id = Id(s1, s2);

				if (min_distance > distance[id]) {
					min_distance = distance[id];
					min_id = id;
					if (min_distance == 1) break;
				}
			}
			if (min_distance == 1) break;
		}

#if LOG_LEVEL == DATA_ANALYSIS
		if (max_of_min_distances < distance[min_id])
			max_of_min_distances = distance[min_id];
		min_distance_counter++;
		min_distance_sum += distance[min_id];
#endif
		//Or changes to starpid will be here, check which is which
		//Ask how to determine starpid
		//apply the path and store it

		//If(min_id < star_dis(Assume Large){do_smt} else {do_smt})
		int pid = min_id;
		while (distance[pid] > 0) {
			int let = letter[pid];
			insertToPath(let, path, last);
			if(last2 != NULL)
			{
				while(last2->next != NULL)
				{
					last2 = last2->next;
				}
			}
			insertToPath(let, globpath, last2);

			for (int i = 0; i < no_actives; i++) {
				actives[i] = a[let * N + actives[i]];
			}

			pid = next[pid];
		}
		
		

		//reduce the number of active states
		int active_count = 0;
		for (int i = 0; i < no_actives; i++) {
			int act = actives[i];
			if (active_marker[act] != step) {
				actives[active_count++] = act;
				active_marker[act] = step;
			}
		}
		no_actives = active_count;
		step++;
	}


/*#ifdef TIMER
	time = omp_get_wtime() - t1;
	total += time;
	cout << "The heuristic takes " << total << " seconds\n";
#if LOG_LEVEL == TIME_ANALYSIS
	out << total << ",";
#endif
#endif*/
#if LOG_LEVEL == DATA_ANALYSIS
	out << "," << (N * (N - 1 )) / 2 << "," << lvl - 1 << ","
	    << max_of_min_distances  << "," << float(min_distance_sum) / min_distance_counter;
#endif
	free(distance);
	free(next);
	free(letter);
	free(que);
}

void applyPath(int* a, PNode* path, int N, int* &actives, int &no_actives) {
	
	PNode* pnode = path;
	while (pnode) {
		int let = pnode->letter;
		for (int i = 0; i < no_actives; i++) {
			actives[i] = a[let * N + actives[i]];
		}
		pnode = pnode->next;
	}

	int* active_marker = new int[N];
	for (int i = 0; i < N; i++) {
		active_marker[i] = 0;
	}

	DynIntStack *stack = generateDynIntStack();
	for (int i = 0; i < no_actives; i++) {
		int act = actives[i];
		if (active_marker[act] != 1) {
			stackPush(stack, act);
			active_marker[act] = 1;
		}
	}

	no_actives = stack->size;

	int *temp = new int[no_actives];
	for (int i = 0; i < no_actives; i++) {
		temp[i] = stackTop(stack); 
		stackPop(stack);
	}

	delete[] actives; actives = temp;

	delete[] active_marker; destroyDynIntStack(stack);
}

int checkInverse(int *a, int* iap, int* ia, int N, int P) {
	for (int p = 0; p < P; p++) {
		for (int i = 0; i < N; i++) {
			int target = a[p * N + i];

			int found = 0;
			for (int iaptr = iap[p * (N + 1) + target]; iaptr < iap[p * (N + 1) + target + 1]; ++iaptr) {
				int incoming = ia[p * N + iaptr];
				
				if (i == incoming) {
					found = 1;
					break;
				}
			}

			if (!found) {
				cout << "something is wrong " << i << " goes to " << target << " with " << p << " but it is not in the inverse automata\n";
				exit(1);
			}
		}
	}

	for (int p = 0; p < P; p++) {
		for (int i = 0; i < N; i++) {
			for (int iaptr = iap[p * (N + 1) + i]; iaptr < iap[p * (N + 1) + i + 1]; ++iaptr) {
				int source = ia[p * N + iaptr];
				if (a[p * N + source] != i) {
					cout << "something is wrong " << i << " has " << source << " in inverse automata but it " << source << " goes to " << a[p * N + source] << " with " << p << "\n";
					exit(1);
				}
			}
		}
	}

	return 0;
}

int main(int argc, char *argv[]) {
	
	int level = 1;
	
	if (argc != 3) {
		cout << "Usage: " << argv[0] << " input_filename output_filename\n" << endl;
		return 1;
	}

	FILE *input_file = fopen(argv[1], "r"); 
	Graph *lastGraph = 0, *graph = 0;
	while((graph = loadGraph(input_file))) {
		if (lastGraph) { destroyGraph(lastGraph); free(lastGraph); }
		lastGraph = graph;
	} graph = lastGraph; //last graph in the file: connected graph
	printf("Graph with %d states, %d inputs and %d SCC(s)\n", graph->states, graph->inputs, graph->SCCs);
	char *scc_name = (char *) malloc(sizeof(char) * (strlen(argv[2]) + 13));
	sprintf(scc_name, "scc_greedy_%s", argv[2]);
	char *greedy_name = (char *) malloc(sizeof(char) * (strlen(argv[2]) + 9));
	sprintf(greedy_name, "greedy_%s", argv[2]);
	FILE *output_scc_file = fopen(scc_name, "w"), *output_greedy_file = fopen(greedy_name, "w"); 
	saveGraph(graph, output_scc_file); fprintf(output_scc_file, "---------\nRESULTS\n---------\n");
	saveGraph(graph, output_greedy_file); fprintf(output_greedy_file, "---------\nRESULTS\n---------\n");

#ifdef LOG_LEVEL
	struct stat st = {0};
	if (stat("results_greedy", &st) == -1) {
		mkdir("results_greedy", 0700);
	}
	char* filename = new char[256];
#if LOG_LEVEL == TIME_ANALYSIS
	sprintf(filename, "results_greedy/timeAndLength_%s_%s.csv", "10", "2");
#elif LOG_LEVEL == DATA_ANALYSIS
	sprintf(filename, "results_greedy/data_%s_%s.csv", "10", "2");
#endif
	if (strcmp("1", "HEADER") == 0) {
		out.open(filename);
		if (level & 1) {
#if LOG_LEVEL == TIME_ANALYSIS
			out << "bfs_time_naive,";
			out << "time_naive,";
			out << "length_naive";
#elif LOG_LEVEL == DATA_ANALYSIS
			out << "allnodes_naive,";
			out << "nodes_naive,";
			out << "level_naive,";
			out << "maxlevel_naive,";
			out << "meanlevel_naive";
#endif
			if ((level ^ 1) > 1)
				out << ",";
		}
		
		out << endl;
		out.close();
		return 0;
	}
	out.open(filename, ios::app);
	free(filename);
#endif

	if (level & 1) {
		long long start = current_timestamp();
		PNode* globpath = NULL;	
		PNode* ptr1 = globpath;

		cout << endl << endl << "SCC METHOD" << endl << endl;

		int *glob_actives = new int[graph->states], glob_no_actives = graph->states;
		for (int i = 0; i < graph->states; i++)
			glob_actives[i] = i;

		PNode* path = NULL;
		Graph **SCC = processSCCs(graph);

		for(int i = graph->SCCs - 1; i >= 0; i--) { 
			if (!SCC[i]) continue;
			int N = SCC[i]->states, P = SCC[i]->inputs;
			int * automata = SCC[i]->data;
		
			printAutomata(automata, N, P);

			int* inv_automata_ptrs = new int[P * (N + 1)];					
			int* inv_automata = new int[P * N];

			//#pragma omp parallel for schedule(static)
			for (int i = 0; i < P; ++i) {
				int *a = &(automata[i * N]);
				int* ia = &(inv_automata[i * N]);
				int* iap = &(inv_automata_ptrs[i * (N + 1)]);

				memset(iap, 0, sizeof(int) * (N + 1));
				for (int j = 0; j < N; j++) {iap[a[j] + 1]++;}
				for (int j = 1; j <= N; j++) {iap[j] += iap[j - 1];}
				for (int j = 0; j < N; j++) {ia[iap[a[j]]++] = j;}
				for (int j = N; j > 0; j--) {iap[j] = iap[j - 1];} iap[0] = 0;
			}

			applyPath(graph->data, path, graph->states, glob_actives, glob_no_actives);
			
			int no = 0;
			for (int j = 0; j < glob_no_actives; j++)
				if (binarySearch(SCC[i]->index, 0, i ? N - 2 : N - 1, glob_actives[j]) != -1)
					no++;

			int* actives = new int[no]; int k = 0;
			for (int j = 0; j < glob_no_actives && k < no; j++) {
				int index = binarySearch(SCC[i]->index, 0, i ? N - 2 : N - 1, glob_actives[j]);
				if (index != -1)
					actives[k++] = index;
			}

			int* active_marker = new int[N];
			for (int i = 0; i < N; i++) {
				active_marker[i] = 0;
			}

			DynIntStack *stack = generateDynIntStack();

			for (int i = 0; i < no; i++) {
				int act = actives[i];
				if (active_marker[act] != 1) {
					active_marker[act] = 1;
					stackPush(stack, act);
				}
			} 
			if (i) stackPush(stack, (SCC[i]->states - 1)); 

			delete[] actives;

			int no_actives = stack->size;
			actives = new int[no_actives];
			for (int i = 0; i < no_actives; i++) {
				actives[i] = stackTop(stack);
				stackPop(stack);
			}

			destroyDynIntStack(stack);

			delete[] active_marker;

			checkInverse(automata, inv_automata_ptrs, inv_automata, N, P);

#ifdef DEBUG	
			printInverseAutomata(inv_automata_ptrs, inv_automata, N, P);
#endif			
			
			delete path; path = NULL;
			greedyHeuristic_naive(automata, inv_automata_ptrs, inv_automata, N, P, path, globpath, actives, no_actives);
			cout << "Path is found: "; 
			PNode* pnode = path;
			int plength = 0;
			while (pnode) {
				cout << pnode->letter << " "; 
				pnode = pnode->next;
				plength++;
			}
			if (i == 0) fprintf(output_scc_file, "PATH: ");
			cout << endl << "Path length is " << plength;
			cout << endl << "After this turn globalpath is : ";
			PNode* pnode2 = globpath;
			int gplength = 0;
			while (pnode2) {
				cout << pnode2->letter << " "; if (i == 0) fprintf(output_scc_file, "%d ", pnode2->letter);
				pnode2 = pnode2->next;
				gplength++;
			}
			if (i == 0) fprintf(output_scc_file, "\nLENGTH: %d\n", gplength);
			cout << endl << "Global Path length is " << gplength << endl;
			//cout << "Number of remaining actives for " << i << "th SCC is " << applyPath(automata, path, N, actives) << endl << endl;

			/*int * lastState = (int *) malloc(sizeof(int) * N);
			for (int i = 0; i < N; i++) lastState[i] =  i;
			PNode* ptr = globpath;
			while(ptr) {
				cout << ptr->letter << ": ";
				for (int i = 0; i < N; i++) {
					lastState[i] = automata[(ptr->letter * N) + lastState[i]];
					cout << lastState[i] << " ";
				}
				cout << endl;
				ptr = ptr->next;
			} free(lastState);*/

			delete[] actives;
	#if LOG_LEVEL == TIME_ANALYSIS
			out << plength;
	#endif
			delete[] inv_automata; delete[] inv_automata_ptrs;
		} long long finish = current_timestamp(); 
		delete[] glob_actives; fprintf(output_scc_file, "TIME: %lld ms\n", finish - start);
		fprintf(output_scc_file, "SCCs: "); for(int i = graph->SCCs - 1; i >= 0; i--) if (!SCC[i]) continue; else fprintf(output_scc_file, "%d ", SCC[i]->states - (i ? 1 : 0));

		fprintf(output_scc_file, "\n---------\nCHECK\n---------\n");
		int * lastState = (int *) malloc(sizeof(int) * graph->states);
		for (int i = 0; i < graph->states; i++) lastState[i] =  i;
		PNode* ptr = globpath;
		while(ptr) {
			fprintf(output_scc_file, "%d: ", ptr->letter);
			for (int i = 0; i < graph->states; i++) {
				lastState[i] = graph->data[(ptr->letter * graph->states) + lastState[i]];
				fprintf(output_scc_file, "%d ", lastState[i]);
			}
			fprintf(output_scc_file, "\n");
			ptr = ptr->next;
		}
		fprintf(output_scc_file, "---------\nEND\n---------\n"); fclose(output_scc_file);
	#ifdef LOG_LEVEL
			if ((level ^ 1) > 1)
				out << ",";
	#endif

		start = current_timestamp();

		int N = graph->states, P = graph->inputs;
		int * automata = graph->data;

#ifdef DEBUG		
		printAutomata(automata, N, P);
#endif
		int* inv_automata_ptrs = new int[P * (N + 1)];					
		int* inv_automata = new int[P * N];

		//#pragma omp parallel for schedule(static)
		for (int i = 0; i < P; ++i) {
			int *a = &(automata[i * N]);
			int* ia = &(inv_automata[i * N]);
			int* iap = &(inv_automata_ptrs[i * (N + 1)]);

			memset(iap, 0, sizeof(int) * (N + 1));
			for (int j = 0; j < N; j++) {iap[a[j] + 1]++;}
			for (int j = 1; j <= N; j++) {iap[j] += iap[j - 1];}
			for (int j = 0; j < N; j++) {ia[iap[a[j]]++] = j;}
			for (int j = N; j > 0; j--) {iap[j] = iap[j - 1];} iap[0] = 0;
		}
			
		int* actives = new int[N];
		for (int i = 0; i < N; ++i) {
			actives[i] = i;
		}

		int no_actives = N;

		checkInverse(automata, inv_automata_ptrs, inv_automata, N, P);

#ifdef DEBUG	
		printInverseAutomata(inv_automata_ptrs, inv_automata, N, P);
#endif

		cout << endl << endl << "GREEDY METHOD" << endl << endl;

		delete path; delete globpath;

		path = NULL; globpath = NULL;
		greedyHeuristic_naive(automata, inv_automata_ptrs, inv_automata, N, P, path, globpath, actives, no_actives);
		cout << "Path is found: "; 
		fprintf(output_greedy_file, "PATH: ");
		PNode* pnode = path;
		int plength = 0;
		while (pnode) {
			cout << pnode->letter << " "; fprintf(output_greedy_file, "%d ", pnode->letter);
			pnode = pnode->next;
			plength++;
		}
		cout << endl << "Path length is " << plength << endl;
		fprintf(output_greedy_file, "\nLENGTH: %d\n", plength);
		applyPath(automata, path, N, actives, no_actives);
		cout << "Number of remaining actives is " << no_actives << endl << endl;
		delete[] actives;

		finish = current_timestamp();
		fprintf(output_greedy_file, "TIME: %lld ms\n", finish - start);
		fprintf(output_greedy_file, "SCCs: "); for(int i = graph->SCCs - 1; i >= 0; i--) fprintf(output_greedy_file, "%d ", graph->SCC[i]->states - (i ? 1 : 0));

		fprintf(output_greedy_file, "\n---------\nCHECK\n---------\n");
		for (int i = 0; i < graph->states; i++) lastState[i] =  i;
		ptr = path;
		while(ptr) {
			fprintf(output_greedy_file, "%d: ", ptr->letter);
			for (int i = 0; i < graph->states; i++) {
				lastState[i] = graph->data[(ptr->letter * graph->states) + lastState[i]];
				fprintf(output_greedy_file, "%d ", lastState[i]);
			}
			fprintf(output_greedy_file, "\n");
			ptr = ptr->next;
		}
		fprintf(output_greedy_file, "---------\nEND\n---------\n"); fclose(output_greedy_file);
		free(lastState);

		}
	#ifdef LOG_LEVEL
		out << endl;
		out.close();
	#endif
	destroyGraph(graph); free(graph);
	printf("Check the files: scc_method: %s greedy_method: %s\n", scc_name, greedy_name);
  free(scc_name);
  free(greedy_name);
	return 0;
}
