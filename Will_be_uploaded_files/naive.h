#ifndef _NAIVE_H_
#define _NAIVE_H_

#include <iostream>
#include <fstream>
#include <iomanip>
#include "global.h"
#include "limits.h"
#include <cfloat>

using namespace std;

enum AlgorithmType { topDown, bottomUp, hybrid };
enum CudaOpt { naive, memory, warp };

template<bool memoryOpt>
void synchronizing_check(int *a, int N, int P, int *distance) {
	int* levels = new int[200];
	memset(levels, 0, 200 * sizeof(int));

	for (int i = 0; i < N; ++i) {
		for (int j = 0; j <= i; ++j) {
			int id;
			if (memoryOpt)
				id = Id(i, j);
			else
				id = IdNaive(i, j, N);

			if (distance[id] == -1) {
				for (int p = 0; p < P; p++) {
					int ts1 = a[p + i * P];
					int ts2 = a[p + j * P];
					int tid;
					if (memoryOpt)
						tid = Id(ts1, ts2);
					else
						tid = IdNaive(ts1, ts2, N);
					cout << "tid " << tid << ": distance is " << distance[tid] << endl;
				}

				cout << "automata is not synchronizing. pair " << id << " - (" << i << ", " << j << ") is not mergable\n";
				out << endl;
				exit(0);
				return;
			}
			else {
				levels[distance[id]]++;
			}
		}
	}

#ifdef DEBUG
	int lvl = 0;
	while (levels[lvl] > 0) {
		cout << "lvl " << lvl++ << ": " << levels[lvl] << endl;
	}
	cout << endl;
#endif

#if LOG_LEVEL & DATA_ANALYSIS
	out << levels[1];
	lvl = 2;
	while (levels[lvl] > 0) {
		out << "-" << levels[lvl++];
	}
#endif
}

template<bool memoryOpt>
long long costPhi(int *a, int *distance, int *letter, int *actives, int *active_marker, int N, int P, int id, int no_actives, int step)
{
	while (distance[id] > 0) {
		int let = letter[id];

		for (int i = 0; i < no_actives; i++) {
			actives[i] = a[let * N + actives[i]];
		}

		int s1, s2;

		if (memoryOpt) {
			s1 = s1fromId(id);
			s2 = s2fromId(id, s1);
			id = Id(a[let * N + s1], a[let * N + s2]);
		}
		else {
			s1 = id / N;
			s2 = id % N;
			id = IdNaive(a[let * N + s1], a[let * N + s2], N);
		}
	}

	//reduce the number of active states
	int active_count = 0;
	for (int i = 0; i < no_actives; i++) {
		int act = actives[i];
		if (active_marker[act] != step) {
			actives[active_count++] = act;
			active_marker[act] = step;
		}
	}
	no_actives = active_count;


	long long finalTotalDist = 0;
	for (int i = 0; i < no_actives; i++) {
		for (int j = 0; j < i; j++) {
			int s1 = actives[i];
			int s2 = actives[j];

			int tid;
			if (memoryOpt)
				tid = Id(s1, s2);
			else
				tid = IdNaive(s1, s2, N);

			finalTotalDist += distance[tid];
		}
	}

	return finalTotalDist;


}

template<bool memoryOpt>
void greedyHeuristic_finding(int *a, int *distance, int *letter, int *actives, int * active_marker, int N, int P, PNode* &path, PNode* &globpath, int no_actives) {

	PNode* last = NULL;
	PNode* last2 = globpath;
	memset(active_marker, 0, sizeof(int) * N);

	int* cp_actives = new int[no_actives];
	int* cp_active_marker = new int[(N * (N + 1)) / 2];
	int step = 1;
	
	while (no_actives > 1) {
		//cout << "no active states is " << no_actives << endl;
		//find the pair id with minimum phi-cost value 

		long long int min_cost = LLONG_MAX;

		int min_id;
		
		for (int i = 0; i < no_actives; i++) {
			for (int j = 0; j < i; j++) {
				int s1 = actives[i];
				int s2 = actives[j];

				int id;
				if (memoryOpt)
					id = Id(s1, s2);
				else
					id = IdNaive(s1, s2, N);
				
				memcpy((void*)cp_actives, (void*)actives, sizeof(int) * no_actives);
				memcpy((void*)cp_active_marker, (void*)active_marker, sizeof(int) * (N * (N + 1)) / 2);
        
        long long int cost = costPhi<memoryOpt>(a, distance, letter, cp_actives, cp_active_marker, N, P, id, no_actives, step);
		
				if (min_cost > cost) {
					min_cost = cost;
					min_id = id;
				}
			}
		}
		

#if LOG_LEVEL & DATA_ANALYSIS
		if (max_of_min_distances < distance[min_id])
			max_of_min_distances = distance[min_id];
		min_distance_counter++;
		min_distance_sum += distance[min_id];
#endif
		// cout << "merging pair from level " << min_distance << endl;

		//apply the path and store it
		int pid = min_id;
		int added = 0;
		while (distance[pid] > 0) {
			int let = letter[pid];
			insertToPath(let, path, last);
			added++;
			if(last2 != NULL)
			{
				while(last2->next != NULL)
				{
					last2 = last2->next;
				}
			}
			insertToPath(let, globpath, last2);

			for (int i = 0; i < no_actives; i++) {
				actives[i] = a[let * N + actives[i]];
			}

			int s1, s2;

			if (memoryOpt) {
				s1 = s1fromId(pid);
				s2 = s2fromId(pid, s1);
				pid = Id(a[let * N + s1], a[let * N + s2]);
			}
			else {
				s1 = pid / N;
				s2 = pid % N;
				pid = IdNaive(a[let * N + s1], a[let * N + s2], N);
			}
		}

		//reduce the number of active states
		int active_count = 0;
		for (int i = 0; i < no_actives; i++) {
			int act = actives[i];
			if (active_marker[act] != step) {
				actives[active_count++] = act;
				active_marker[act] = step;
			}
		}
		no_actives = active_count;
		step++;
	}


	delete[] cp_actives;
	delete[] cp_active_marker;
}

//a is automata a[i][j] -> state j goes to a[i][j] with letter j
//iap is inverse automata pointers -> ia[i][iap[i][j] ... ia[i][j+1]] keeps the state ids which go to state j with letter i
//there are N states and p letters in the automata
void greedyHeuristic_naive(int* a, int* iap, int* ia, int N, int P, PNode* &path, PNode* &globpath, int* actives, int no_actives) {
	
	int noOfPair = (N * (N + 1)) / 2;
	int* active_marker = new int[noOfPair];

	int* distance = new int[noOfPair];
	int* letter = new int[noOfPair];
	int* que = new int[noOfPair];
	int* next = new int[noOfPair];


/*#ifdef TIMER
	double t1 = omp_get_wtime();
	double total = 0;
#endif*/

#if LOG_LEVEL & DATA_ANALYSIS
	int max_of_min_distances = 0;
	int min_distance_counter = 0;
	int min_distance_sum = 0;
#endif


	for (int i = 0; i < noOfPair; i++) {
		distance[i] = -1;
	}

	//BFS queue for the pairs
	int qs = 0;
	int qe = 0;

	for (int i = 0; i < N; ++i) {
		int id = Id(i, i);
		distance[id] = 0;
		que[qe++] = id;
	}
	
	//there are more nodes in the queue
	while (qs < qe) {
		int q_id = que[qs++];
		int q_dist = distance[q_id];

		//will process the pair with id q_id now
		int q_s1 = s1fromId(q_id); //the first state in the pair
		int q_s2 = s2fromId(q_id, q_s1); //the second state in the pair (we are sure that q_s1 >= q_s2)

#ifdef DEBUG
		cout << "will process " << q_s1 << " " << q_s2 << " with id  " << q_id << " with distance " << q_dist << endl;
#endif

		int* p_ia = ia; //this is the inverse automata for letter p
		int* p_iap = iap; //and its state pointers

		for (int p = 0; p < P; p++) {

			for (int iap_s1_ptr = p_iap[q_s1]; iap_s1_ptr < p_iap[q_s1 + 1]; ++iap_s1_ptr) {
				int ia_s1 = p_ia[iap_s1_ptr];
				for (int iap_s2_ptr = p_iap[q_s2]; iap_s2_ptr < p_iap[q_s2 + 1]; ++iap_s2_ptr) {
					int ia_s2 = p_ia[iap_s2_ptr];
					int ia_id = Id(ia_s1, ia_s2);
					if (distance[ia_id] < 0) { //we found an unvisited pair. so we need to add this to the queue
						distance[ia_id] = q_dist + 1;
						letter[ia_id] = p;
						que[qe++] = ia_id;
					}
				}
			}
			p_ia += N; //this is the inverse automata for letter p
			p_iap += (N + 1); //and its state pointers
		}
	}
/*#ifdef TIMER
	double time = omp_get_wtime() - t1;
	total += time;
	cout << "BFS tree generation takes " << time << " seconds\n";
#if LOG_LEVEL & TIME_ANALYSIS
	out << time << " ";
#endif
#endif*/
	
	synchronizing_check<true>(a, N, P, distance);
	
/*#ifdef TIMER
	t1 = omp_get_wtime();
#endif*/

	greedyHeuristic_finding<true>(a, distance, letter, actives, active_marker, N, P, path, globpath, no_actives);

/*#ifdef TIMER
	time = omp_get_wtime() - t1;
	cout << "Path generation takes " << time << " seconds\n";
	total += time;
	cout << "The heuristic takes " << total << " seconds\n";
#if LOG_LEVEL & TIME_ANALYSIS
	out << total << " ";
#endif
#endif*/
#if LOG_LEVEL & DATA_ANALYSIS
	out << " " << (N * (N - 1)) / 2 << " " << lvl - 1 << " "
		<< max_of_min_distances << " " << float(min_distance_sum) / min_distance_counter;
#endif
	free(distance);
	free(letter);
	free(que);
}


#endif //_NAIVE_H_
