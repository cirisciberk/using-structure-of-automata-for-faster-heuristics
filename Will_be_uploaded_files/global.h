#ifndef _GLOBAL_
#define _GLOBAL_

#include <iostream>
#include <queue>
#include "graph.h"
using namespace std;

#define TIMER
//#define DEBUG
#ifdef TIMER
#define LEVEL_ANALYSIS 4
#define TIME_ANALYSIS 1 //#define TIMER macro is required for time analysis
#endif
#define DATA_ANALYSIS 2
#define LOG_LEVEL TIME_ANALYSIS


#define DIV(X, Y) (1+((X-1)/Y))

#ifdef LOG_LEVEL
ofstream out;
#endif

struct PNode {
	int letter;
	PNode* next;
	PNode(int _letter, PNode* _next) : letter(_letter), next(_next) {}
};

#define Id(s1, s2) ((s1 > s2)?(((s1 * (s1 + 1))/2) + s2):(((s2 * (s2 + 1))/2) + s1)) //this is how we compute the ids
#define IdNaive(s1, s2, N) ((s1 > s2)?((s1 * N) + s2):((s2 * N) + s1)) //this is how we compute the ids
#define s1fromId(id) ((int)(sqrt((2.0 * id) +1.0) - 0.5));
#define s2fromId(id, s1) (id - ((s1 * (s1 + 1))/2));

void insertToPath(int letter, PNode* &head, PNode* &last) {
	PNode* temp = new PNode(letter, NULL);
	if (head == NULL) {
		head = last = temp;
	} else {
		last = last->next = temp;
	}
}

void printAutomata(int* automata, int N, int p) {
	cout << "Automata ----------------------" << endl;
	for (int i = 0; i < p; ++i) {
		cout << "letter " << (char)(i + 97) << ":\t";
		for (int j = 0; j < N; ++j) {
			cout << automata[i * N + j] << "\t";
		}
		cout << endl;
	}
}

void printInverseAutomata(int* inv_automata_ptrs, int* inv_automata, int N, int p) {
	cout << "Inverse Automata --------------" << endl;
	for (int i = 0; i < p; ++i) {
		cout << "letter " << (char)(i + 97) << ":\n";
		/* for (int j = 0; j <= N; ++j) {
			cout << inv_automata_ptrs[i * (N + 1) + j] << "\t";
		} */
		cout << endl;
		for (int j = 0; j < N; ++j) {
			cout << inv_automata[i * N + j] << "\t";
		}
		cout << endl << endl;
	}
}

void applyPath(int* a, PNode* path, int N, int* &actives, int &no_actives) {
	
	PNode* pnode = path;
	while (pnode) {
		int let = pnode->letter;
		for (int i = 0; i < no_actives; i++) {
			actives[i] = a[let * N + actives[i]];
		}
		pnode = pnode->next;
	}

	int* active_marker = new int[N];
	for (int i = 0; i < N; i++) {
		active_marker[i] = 0;
	}

	DynIntStack *stack = generateDynIntStack();
	for (int i = 0; i < no_actives; i++) {
		int act = actives[i];
		if (active_marker[act] != 1) {
			stackPush(stack, act);
			active_marker[act] = 1;
		}
	}

	no_actives = stack->size;

	int *temp = new int[no_actives];
	for (int i = 0; i < no_actives; i++) {
		temp[i] = stackTop(stack); 
		stackPop(stack);
	}

	delete[] actives; actives = temp;

	delete[] active_marker; destroyDynIntStack(stack);
}

#endif