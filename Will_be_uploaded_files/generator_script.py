#!/usr/bin/python
import os
import sys
import re
import random
from math import floor


class Node(object):
    def __init__(self, data):
        self.data = data
        self.children = []

    def add_child(self, obj):
        self.children.append(obj)

def randomSCC(numSCC):
    connectToSCC = random.randint(0,numSCC)
    return connectToSCC




#SCC start from 0 to len-1
def creator():  # filename, seed(?), input count, scc count, scc state sizes, methodnum
    stateSize1 = [32, 64, 128]
    sccNum1 = [2, 4, 8]
    inputSize = [2, 4, 8]
    distFactor = [0.25, 0.5, 0.75]
    experimentSize = 51

    # stateSize1 = [128, 256, 512]
    # sccNum1 = [4, 8, 16]
    # sccNum2 = [3, 7, 15]
    # inputSize = [2, 4, 8]
    # distFactor = [0.25, 0.5, 0.75]
    # experimentSize = 51

    testresults = open("scc_extended_test_results.csv","w")
    testresults.write("sep=,\n")
    testresults.write("Type,States,Inputs,SCC,distFactor,Seed,SP Length,SP Time(ms),SPSCC Length,SPSCC Time(ms),G Length,G Time(ms),GSCC Length,GSCC Time(ms)\n")

    count = 0
    for s in stateSize1:
        for p in inputSize:
            for c in sccNum1:
                for r in distFactor:
                    for expID in range(1, experimentSize):
                        print(str(count) + "th file is being processed.")
                        count += 1
                        # linear connection
                        connections = ""
                        # c-1 because we wont do anything with first scc
                        for i_1 in range(c-1):
                            if i_1 == 0:
                                connections = str(
                                    c - (i_1+1)) + " " + str(r) + " " + str(c - (i_1+2))
                            else:
                                connections = connections + " " + \
                                    str(c - (i_1+1)) + " " + \
                                    str(r) + " " + str(c - (i_1+2))
                        # first number is for showing which method of connection is using.
                        testresults.write("linear" + "," + str(s) + "," + str(p) + "," + str(c) + "," + str(r) + "," + str(expID*2) + ",")
                        filename = "0" + "_" + \
                            str(s) + "_" + str(p) + "_" + str(c) + "_" + \
                            str(int(r*100)) + "_" + str(expID*2) + ".txt"
                        if not os.path.exists(filename):
                            cmd = "./generator " + filename + " " + \
                                str(expID*2) + " " + str(p) + " " + \
                                str(c) + " " + (str(s) + " ") *c  + connections
                            os.system(cmd + " > /dev/null")
                        
                        if not os.path.exists("greedy_" + filename) or not os.path.exists("scc_greedy_" + filename):

                            cmd = "./greedy " + filename + " " + filename
                            os.system(cmd + " > /dev/null")

                        if not os.path.exists("synchroP_" + filename) or not os.path.exists("scc_synchroP_" + filename):
                            cmd = "./heuristics " + filename + " " + filename
                            os.system(cmd + " > /dev/null")
                        if os.path.exists("synchroP_" + filename) and os.path.exists("scc_synchroP_" + filename):
	                        f = open("synchroP_" + filename)   
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
	                        f = open("scc_synchroP_" + filename)
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
                    	if os.path.exists("greedy_" + filename) and os.path.exists("scc_greedy_" + filename):
	                        f = open("greedy_" + filename)
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
	                        f = open("scc_greedy_" + filename)
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
                        # connect scc to previous
                        testresults.write("\nprevious" + "," + str(s) + "," + str(p) + "," + str(c) + "," + str(r) + "," + str(expID*2) + ",")
                        connections2 = ""
                        # because it starts from 0 and we want it to be connected in a backwards manner
                        for i_2 in range(c-1):
                            fraction = r/(c-i_2-1)
                            for k in range(c-(i_2+1)):
                                if i_2 == 0 and k == 0:
                                    connections2 = str(c-i_2-1) + " " + str(fraction) + " " + str(k)
                                else:
                                    connections2 = connections2 + " " + \
                                        str(c-i_2-1) + " " + str(fraction) + " " + str(k)
                        # first number is for showing which method of connection is using.
                        filename = "1" + "_" + \
                            str(s) + "_" + str(p) + "_" + str(c) + "_" + \
                            str(int(r*100)) + "_" + str(expID*2) + ".txt"
                        
                        if not os.path.exists(filename):
                            cmd = "./generator " + filename + " " + \
                                str(expID*2) + " " + str(p) + " " + \
                                str(c) + " " + (str(s) + " ") *c  + connections2
                            os.system(cmd + " > /dev/null")
                        
                        if not os.path.exists("greedy_" + filename) or not os.path.exists("scc_greedy_" + filename):

                            cmd = "./greedy " + filename + " " + filename
                            os.system(cmd + " > /dev/null")

                        if not os.path.exists("synchroP_" + filename) or not os.path.exists("scc_synchroP_" + filename):
                            cmd = "./heuristics " + filename + " " + filename
                            os.system(cmd + " > /dev/null")
                        if os.path.exists("synchroP_" + filename) and os.path.exists("scc_synchroP_" + filename):
	                        f = open("synchroP_" + filename)   
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
	                        f = open("scc_synchroP_" + filename)
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
                       	if os.path.exists("greedy_" + filename) and os.path.exists("scc_greedy_" + filename):
	                        f = open("greedy_" + filename)
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
	                        f = open("scc_greedy_" + filename)
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
                        # connect all scc to first
                        
                        testresults.write("\nfirst" + "," + str(s) + "," + str(p) + "," + str(c) + "," + str(r) + "," + str(expID*2) + ",")
                        connections3 = ""
                        for i_3 in range(c-1):
                            if i_3 == 0:
                                connections3 = str(c-(i_3+1)) + " " + str(r) + " " + "0"
                            else:
                                connections3 = connections3 + " " + \
                                    str(c-(i_3+1)) + " " + str(r) + " " + "0"
                        # first number is for showing which method of connection is using.
                        filename = "2" + "_" + \
                            str(s) + "_" + str(p) + "_" + str(c) + "_" + \
                            str(int(r*100)) + "_" + str(expID*2) + ".txt"
                        
                        if not os.path.exists(filename):
                            cmd = "./generator " + filename + " " + \
                                str(expID*2) + " " + str(p) + " " + \
                                str(c) + " " + (str(s) + " ") *c  + connections3
                            os.system(cmd + " > /dev/null")
                        
                        if not os.path.exists("greedy_" + filename) or not os.path.exists("scc_greedy_" + filename):

                            cmd = "./greedy " + filename + " " + filename
                            os.system(cmd + " > /dev/null")

                        if not os.path.exists("synchroP_" + filename) or not os.path.exists("scc_synchroP_" + filename):
                            cmd = "./heuristics " + filename + " " + filename
                            os.system(cmd + " > /dev/null")
                        if os.path.exists("synchroP_" + filename) and os.path.exists("scc_synchroP_" + filename):
	                        f = open("synchroP_" + filename)   
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
	                        f = open("scc_synchroP_" + filename)
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
                        if os.path.exists("greedy_" + filename) and os.path.exists("scc_greedy_" + filename):
	                        f = open("greedy_" + filename)
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
	                        f = open("scc_greedy_" + filename)
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
                            
                        #Random Connection
                        #Check if works
                        testresults.write("\nrandom" + "," + str(s) + "," + str(p) + "," + str(c) + "," + str(r) + "," + str(expID*2) + ",")
                        connections5 =""                        
                        for i_4 in range(c-1):
                            rand = randomSCC(c-(i_4+2))
                            if i_4 == 0:
                                connections5 = str(c-(i_4+1)) + " " + str(r) + " " + str(rand)
                            else:
                                connections5 = connections5 + " " + \
                                    str(c-(i_4+1)) + " " + str(r) + " " + str(rand)
                        filename = "4" + "_" + \
                            str(s) + "_" + str(p) + "_" + str(c) + "_" + \
                            str(int(r*100)) + "_" + str(expID*2) + ".txt"
                        
                        if not os.path.exists(filename):
                            cmd = "./generator " + filename + " " + \
                                str(expID*2) + " " + str(p) + " " + \
                                str(c) + " " + (str(s) + " ") *c  + connections5
                            os.system(cmd + " > /dev/null")
                        
                        if not os.path.exists("greedy_" + filename) or not os.path.exists("scc_greedy_" + filename):

                            cmd = "./greedy " + filename + " " + filename
                            os.system(cmd + " > /dev/null")

                        if not os.path.exists("synchroP_" + filename) or not os.path.exists("scc_synchroP_" + filename):
                            cmd = "./heuristics " + filename + " " + filename
                            os.system(cmd + " > /dev/null")
                        if os.path.exists("synchroP_" + filename) and os.path.exists("scc_synchroP_" + filename):
	                        f = open("synchroP_" + filename)   
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
	                        f = open("scc_synchroP_" + filename)
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
                        if os.path.exists("greedy_" + filename) and os.path.exists("scc_greedy_" + filename):
	                        f = open("greedy_" + filename)
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
	                        f = open("scc_greedy_" + filename)
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
                        #tree
                        connections4 = ""
                        for i in range(2*c-2, 0, -1):
                            connections4 += str(i) + " " + str(r) + " " + str((i+1)//2-1) + " "

                        testresults.write("\ntree" + "," + str(s) + "," + str(p) + "," + str(2*c-1) + "," + str(r) + "," + str(expID*2) + ",")
                        filename = "3" + "_" + \
                            str(s) + "_" + str(p) + "_" + str(2*c-1) + \
                            "_" + str(int(r*100)) + "_" + str(expID*2) + ".txt"
                        if not os.path.exists(filename):
                            cmd = "./generator " + filename + " " + \
                                str(expID*2) + " " + str(p) + " " + \
                                str(2*c-1) + " " + (str(s) + " ") *(2*c-1)  + connections4
                            os.system(cmd + " > /dev/null")
                        
                        if not os.path.exists("greedy_" + filename) or not os.path.exists("scc_greedy_" + filename):

                            cmd = "./greedy " + filename + " " + filename
                            os.system(cmd + " > /dev/null")

                        if not os.path.exists("synchroP_" + filename) or not os.path.exists("scc_synchroP_" + filename):
                            cmd = "./heuristics " + filename + " " + filename
                            os.system(cmd + " > /dev/null")
                        if os.path.exists("synchroP_" + filename) and os.path.exists("scc_synchroP_" + filename):
	                        f = open("synchroP_" + filename)   
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
	                        f = open("scc_synchroP_" + filename)
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
                        if os.path.exists("greedy_" + filename) and os.path.exists("scc_greedy_" + filename):
	                        f = open("greedy_" + filename)
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
	                        f = open("scc_greedy_" + filename)
	                        for line in f:
	                            if re.match("LENGTH: ", line):
	                                testresults.write(line[8:-1] + ",")
	                            if re.match("TIME: ", line):  
	                                testresults.write(line[6:-4] + ",")
	                        f.close()
                        testresults.write("\n")
    testresults.close()

def main():

    creator()


if __name__ == "__main__":
    main()