#!/usr/bin/python
import os
import sys
import re

def tester(stateSize, inputSize, SCC, experimentSize):
   n = 0 
   testresults = open("testresults_greedy3.csv","w")
   testresults.write("sep=,\n")
   testresults.write("Total States,States,Inputs,SCC,Seed,SCC Length,SCC Time(ms),Algo Length,Algo Time(ms)\n")
   for s in stateSize:
     for p in inputSize:
       for c in SCC:
         for expID in range(1, experimentSize):   
            print "states: ", s, "inputs: ", p, "SCC: ", c, "seed: ", expID*2
               
            ifilename = str(s) + "_" + str(p) + "_" + str(c) + "_" + str(expID*2) + ".txt"
            ofilename = str(s) + "_" + str(p) + "_" + str(c) + "_" + str(expID*2) + "_results.txt"
              
            states = ""
            for c2 in range(c):
              states = states + str(s/c) + " "   
            sccresult = "scc_" + ofilename
            syncresult = "greedy_" + ofilename
            testresults.write(str(s) + "," + states + "," + str(p) + "," + str(c)  + "," + str(expID*2) + ",")
                   
            cmd = "./generator " + ifilename + " " + str(expID*2) + " " + str(p) + " " + states
            os.system(cmd + " > /dev/null")
            cmd = "./greedy " + ifilename + " " + ofilename
            #os.system(cmd)
            os.system(cmd + " > /dev/null")       
            f = open(sccresult, "r")
            for line in f:
              if re.match("LENGTH: ", line):
                testresults.write(line[8:-1] + ",")
              if re.match("TIME: ", line):  
                testresults.write(line[6:-4] + ",")
            f.close()
              
            f = open(syncresult, "r")
            for line in f:
              if re.match("LENGTH: ", line):
                  testresults.write(line[8:-1] + ",")
              if re.match("TIME: ", line):  
                  testresults.write(line[6:-4]) 
            testresults.write("\n")    
            f.close()
            n = n+1
            print("%" + str((100*n)/(3*3*3*50)))
     
   testresults.close()    

def main():
    stateSize = [256, 512, 1024 ,2048]
    inputSize = [2, 4, 8]
    SCC = [2, 4, 8]
    experimentSize = 51
    tester(stateSize, inputSize, SCC, experimentSize)
    
    
#    stateSize = [256]
#    inputSize = [2]
#    SCC = [8]
#    distFactor = [0]
#    experimentSize = 5
#    tester(stateSize, inputSize, SCC, distFactor, experimentSize)
       
if __name__ == "__main__":
    main()
