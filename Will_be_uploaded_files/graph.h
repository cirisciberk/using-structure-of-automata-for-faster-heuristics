#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>

long long current_timestamp();
void swap(int *, int *);
void bubbleSort(int *, int);
int binarySearch(int *, int, int, int);
int power(int, int);
int isAllSame(int *, int, int);
int isExists(int *, int, int);

typedef struct DynIntList {
		int val;
		struct DynIntList *next;
} DynIntList;

DynIntList *generateDynIntList(int);
void destroyDynIntList(DynIntList *);

typedef struct DynIntStack {
		int size;
		DynIntList *data;
} DynIntStack;

DynIntStack *generateDynIntStack();
void destroyDynIntStack(DynIntStack *);
int stackEmpty(DynIntStack *);
int stackTop(DynIntStack *);
void stackPop(DynIntStack *);
void stackPush(DynIntStack *, int);

typedef struct Graph {
		int states;
		int inputs;
		int SCCs;
		struct Graph ** SCC;
		int * data;
		int * index; //which states are in the graph, for SCC graphs
		int *** SCCrel; //matrix to store the relation between SCCs, #s are according to topological sort
		int * stateSCCs; //stores which SCC a state belongs to
} Graph;

Graph *generateGraph(int, int);
Graph *copyGraph(Graph *);
void shuffle(int *array, size_t n);
void destroyGraph(Graph *);
void fillGraph(Graph *, int);
int *getIEs(Graph *, int, int);
int *getIEsNew(Graph *, int, int, int*, int*);
Graph *connectGraph(Graph *, Graph *);
Graph *connectGraphs(Graph **, int);
Graph *joinSCCs(Graph *);
void saveGraph(Graph *, FILE *);
void ControlledSCCconnector(Graph **gArr, char* args[], int argc, Graph* last);
Graph *loadGraph(FILE *);
int inSCC(int, Graph *);
Graph **processSCCs(Graph *);

void DFSUtil(Graph *, int, int, int *);
int DFS(Graph *, int, int);

void SCCUtil(Graph *, int, int *, int *, DynIntStack *, int *, int *);
void SCC(Graph *);