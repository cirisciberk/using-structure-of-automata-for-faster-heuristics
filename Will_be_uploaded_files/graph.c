#include "graph.h"

long long current_timestamp() {
    struct timeval te; 
    gettimeofday(&te, 0); // get current time
    long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // caculate milliseconds
    return milliseconds;
}

// A utility function to swap two elements
void swap(int* a, int* b)
{
    int t = *a;
    *a = *b;
    *b = t;
}
 
// A function to implement bubble sort
void bubbleSort(int *arr, int n)
{
   int i, j;
   for (i = 0; i < n-1; i++)      
 
       // Last i elements are already in place   
       for (j = 0; j < n-i-1; j++) 
           if (arr[j] > arr[j+1])
              swap(&arr[j], &arr[j+1]);
}

// A recursive binary search function. It returns location of x in
// given array arr[l..r] is present, otherwise -1
int binarySearch(int *arr, int l, int r, int x)
{
   if (r >= l)
   {
        int mid = l + (r - l)/2;
 
        // If the element is present at the middle itself
        if (arr[mid] == x)  return mid;
 
        // If element is smaller than mid, then it can only be present
        // in left subarray
        if (arr[mid] > x) return binarySearch(arr, l, mid-1, x);
 
        // Else the element can only be present in right subarray
        return binarySearch(arr, mid+1, r, x);
   }
 
   // We reach here when element is not present in array
   return -1;
}

int power(int base, int exp){
        int result = 1;
        while(exp){
                if(exp & 1)
                        result *= base;
                exp >>= 1;
                base *= base;
        }
	return result;
}

/********************************************************************
*  Checks if all the elements in data are the number find or not.	*
* if one of them is not find: returns 0 without looking further,    *
* if all find: returns 1.											*
*********************************************************************/
int isAllSame(int *data, int size, int find) {
	int i;
	for (i = 0; i < size; i++)
		if (data[i] != find)
			return 0;
	return 1;
}

int isExists(int *data, int size, int find) {
	int i;
	for (i = 0; i < size; i++)
		if (data[i] == find)
			return 1;
	return 0;
}

/**************************************
* Functions related to DynIntStack.   *
***************************************/

DynIntStack *generateDynIntStack() {
	DynIntStack *stack = (DynIntStack *) malloc(sizeof(DynIntStack));
	stack->size = 0;
	stack->data = 0;
	return stack;
}

void destroyDynIntStack(DynIntStack *stack) {
	stack->size = 0;
	destroyDynIntList(stack->data);
	stack->data = 0;
}

int stackEmpty(DynIntStack *stack) {
	if (!stack->size || !stack->data) return 1;
	return 0;
}

int stackTop(DynIntStack *stack) {
	return stack->data->val;
}

void stackPop(DynIntStack *stack) {
	if (!stack->size || !stack->data) return;
	DynIntList *temp = stack->data; 
	stack->data = temp->next; temp->next = 0;
	destroyDynIntList(temp); stack->size--;
}

void stackPush(DynIntStack *stack, int val) {
	DynIntList *temp = generateDynIntList(val); 
	if (stack->data) temp->next = stack->data;
	stack->data = temp; stack->size++;
}

/************************************************
* Constructor for dynamic integer linked list. *
*************************************************/
DynIntList *generateDynIntList(int val) {
	DynIntList *head = (DynIntList *) malloc(sizeof(DynIntList));
	head->val = val;
	head->next = 0;
	return head;
}

/************************************************
*  Destructor for dynamic integer linked list.  *
*************************************************/
void destroyDynIntList(DynIntList *head) {
	DynIntList *temp = head;
	while(temp) {
		temp = temp->next;
		free(head);
		head = temp;
	}
}

/***************************************************************
* As the name implies, turns an integer stack into an integer. *
****************************************************************/
int toInteger(DynIntStack *stack) {
	int i = 0, num = 0;
	while (!stackEmpty(stack)) {
		num += stackTop(stack) * power(10, i++);
		stackPop(stack);
	}
	return num;
}


void printGraph(Graph *graph)
{
  int i, j;
	for (i = 0; i < graph->inputs; i++)
	{
		printf("\nletter %d\n", i );
		for(j = 0; j < graph->states; j++)
		{
			printf("%d ", graph->data[i * graph->states + j]);

		}
	}
}


/***********************************
*  Constructor for struct Graph,   *
* makes all the necessary memory   *
* allocations and initializations. *
************************************/
Graph *generateGraph(int states, int inputs) {
	int i, j;
	Graph *graph = (Graph *) malloc(sizeof(Graph));
	graph->states = states;
	graph->inputs = inputs;
	graph->SCCs = 0;
	graph->SCC = (Graph **) malloc(sizeof(Graph *) * states);
	graph->data = (int *) malloc(sizeof(int) * states * inputs);
	graph->index = 0; 
	graph->SCCrel = 0; 
	graph->stateSCCs = 0; 
	for (i = 0; i < states * inputs; i++) {
		graph->data[i] = -1;
		if (i < states) graph->SCC[i] = 0;
	}
	return graph;
}

#define Id(s1, s2) ((s1 > s2)?(((s1 * (s1 + 1))/2) + s2):(((s2 * (s2 + 1))/2) + s1)) //this is how we compute the ids
#define s1fromId(id) ((int)(sqrt((2.0 * id) +1.0) - 0.5));
#define s2fromId(id, s1) (id - ((s1 * (s1 + 1))/2));

Graph *generatePairGraph(Graph *graph)
{
  int i, j, k;
	int N = graph->states;
	int P = graph->inputs;
	int no_Pairs = (N * (N + 1)) / 2;

	Graph *pairGraph = generateGraph(no_Pairs, P);

	for(j = 0; j < P; j++)
	{	for(i = 0; i < N; i++)
		{	for(k = i; k < N; k++)
			{
				int s1 = graph->data[j * N + i];
				int s2 = graph->data[j * N + k];

				int id = Id(k, i);
				int to_id = Id(s2, s1);
				pairGraph->data[j * no_Pairs + id] = to_id;

			} 
		}
	}
	return pairGraph;
}
/********************************************
*  Deep copy constructor for struct Graph.	*
*********************************************/
Graph *copyGraph(Graph *copy) {
	if (!copy) return 0;
	int i, j, k;
	Graph *graph = (Graph *) malloc(sizeof(Graph));
	graph->states = copy->states;
	graph->inputs = copy->inputs;
	graph->SCCrel = 0; 
	graph->stateSCCs = 0; graph->index = 0;
	graph->SCCs = copy->SCCs;
	graph->SCC = (Graph **) malloc(sizeof(Graph *) * graph->states);
	for (i = 0; i < graph->states; i++)
		if (copy->SCC[i])
			if (copy->SCC[i] == copy || copy->SCCs == 1)
				graph->SCC[i] = graph;
			else
				graph->SCC[i] = copyGraph(copy->SCC[i]);
		else
			graph->SCC[i] = 0;
	if (copy->stateSCCs) {
		graph->stateSCCs = (int *) malloc(sizeof(int) * graph->states);
		for (i = 0; i < graph->states; i++)
			graph->stateSCCs[i] = copy->stateSCCs[i];
	}
	if (copy->SCCrel) {
		graph->SCCrel = (int ***) malloc(sizeof(int **) * graph->SCCs);
		for (i = 0; i < graph->SCCs; i++) {
			graph->SCCrel[i] = (int **) malloc(sizeof(int *) * graph->SCCs);
			for (j = 0; j < graph->SCCs; j++) {
				graph->SCCrel[i][j] = (int *) malloc(sizeof(int) * graph->inputs);
				for (k = 0; k < graph->inputs; k++)
					graph->SCCrel[i][j][k] = copy->SCCrel[i][j][k];
			}
		}
	}
	if (copy->index) {
		graph->index = (int *) malloc(sizeof(int) * (graph->states - 1 ));
		for (i = 0; i < (graph->states - 1); i++)
			graph->index[i] = copy->index[i];
	}
	graph->data = (int *) malloc(sizeof(int) * graph->states * graph->inputs);
	for (i = 0; i < graph->states * graph->inputs; i++)
		graph->data[i] = copy->data[i];
	return graph;
}

/*********************************************
*	Assign the second graph to first graph,  *
*  since there is no pass-by-reference in C  *
*  this function is a must to avoid use of   *
*  				double pointers.			 *
**********************************************/
void assignGraph(Graph *graph1, Graph *graph2) {
	if (!graph2) return;
	int i, j, k;
	destroyGraph(graph1);
	graph1->states = graph2->states;
	graph1->inputs = graph2->inputs;
	graph1->SCCrel = 0; 
	graph1->stateSCCs = 0; graph1->index = 0;
	graph1->SCCs = graph2->SCCs;
	graph1->SCC = (Graph **) malloc(sizeof(Graph *) * graph1->states);
	for (i = 0; i < graph1->states; i++)
		if (graph2->SCC[i])
			if (graph2->SCC[i] == graph2 || graph2->SCCs == 1)
				graph1->SCC[i] = graph1;
			else
				graph1->SCC[i] = copyGraph(graph2->SCC[i]);
		else
			graph1->SCC[i] = 0;
	if (graph2->stateSCCs) {
		graph1->stateSCCs = (int *) malloc(sizeof(int) * graph1->states);
		for (i = 0; i < graph1->states; i++)
			graph1->stateSCCs[i] = graph2->stateSCCs[i];
	}
	if (graph2->SCCrel) {
		graph1->SCCrel = (int ***) malloc(sizeof(int **) * graph1->SCCs);
		for (i = 0; i < graph1->SCCs; i++) {
			graph1->SCCrel[i] = (int **) malloc(sizeof(int *) * graph1->SCCs);
			for (j = 0; j < graph1->SCCs; j++) {
				graph1->SCCrel[i][j] = (int *) malloc(sizeof(int) * graph1->inputs);
				for (k = 0; k < graph1->inputs; k++)
					graph1->SCCrel[i][j][k] = graph2->SCCrel[i][j][k];
			}
		}
	}
	if (graph2->index) {
		graph1->index = (int *) malloc(sizeof(int) * (graph1->states - 1));
		for (i = 0; i < (graph1->states - 1); i++)
			graph1->index[i] = graph2->index[i];
	}
	graph1->data = (int *) malloc(sizeof(int) * graph1->states * graph1->inputs);
	for (i = 0; i < graph1->states * graph1->inputs; i++)
		graph1->data[i] = graph2->data[i];
}

/*********************************
*  Destructor for struct Graph.	 *
* Destroy the given graph,       *
*     without freeing itself.	 *	  
**********************************/
void destroyGraph(Graph *graph) {
	if (!graph) return;
	int i, j;
	for (i = 0; i < graph->states; i++)
		if (graph->SCC[i] && graph != graph->SCC[i]) {
			destroyGraph(graph->SCC[i]); free(graph->SCC[i]);
		}
	free(graph->SCC);
	free(graph->data);
	free(graph->index);
	free(graph->stateSCCs);
	if (graph->SCCrel)
		for(i = 0; i < graph->SCCs; i++) {
			for(j = 0; j < graph->SCCs; j++)
				free(graph->SCCrel[i][j]);
			free(graph->SCCrel[i]);
		}
	free(graph->SCCrel);
}

/************************************************
*	Fills the given graph, using random inputs  *
*  based on # of states, if it's not strongly   *
*  connected as a whole then merge it to be SC. *
*************************************************/
void fillGraph(Graph *graph, int isRandom) {
	int i, j; Graph *temp = 0;
	for (i = 0; i < graph->states * graph->inputs; i++)
			graph->data[i] = rand() % graph->states;
	if (isRandom) { SCC(graph); }
	else {
		assignGraph(graph, temp = joinSCCs(graph));
		destroyGraph(temp); free(temp);
	}
}

/********************************************************
*	Finds at most limit # of ineffective edges	 		*
*  on the given graph and returns them at a dynamic		*
*  array whose size is states * inputs, unused ones are *
*  				initialized as -1.						*
*********************************************************/
int *getIEs(Graph *graph, int whichSCC, int limit) {
	int i, j, k = 0;
	int *iEs = (int *) malloc(sizeof(int) * (graph->states * graph->inputs));
	Graph *temp = copyGraph(graph);  
  int loopLimit = (whichSCC ? temp->SCC[whichSCC]->states - 1 : temp->SCC[whichSCC]->states);
  //printf("Temp States: %d\n", temp->states);
	for (i = 0; i  < loopLimit; i++) {  
		int state = graph->SCC[whichSCC]->index[i];
    //printf("State: %d\n", state);
		for (j = 0; j < temp->inputs; j++) {
			int input = temp->data[(j * temp->states) + state]; 
			temp->data[(j * temp->states) + state] = temp->states;
			if (DFS(temp, state, input)) iEs[k++] = (j * temp->states) + state;
			else temp->data[(j * temp->states) + state] = input;
      //printf("k: %d\n", k);
			if (limit && k >= limit) break;
		}
		if (limit && k >= limit) break;
	}
	destroyGraph(temp); free(temp); temp = 0;
	for (; k < graph->states * graph->inputs; k++) iEs[k] = -1;
	return iEs;
}

/********************************************************
*	Finds at most limit # of ineffective edges	 		*
*  on the given graph and returns them at a dynamic		*
*  array whose size is states * inputs, unused ones are *
*  initialized as -1. Edges selection is unbiased.		*
*********************************************************/

int *getIEsNew(Graph *graph, int whichSCC, int limit, int *x, int*list) {
	int i, j, k = 0;
	int *iEs = (int *) malloc(sizeof(int) * (graph->states * graph->inputs));
	
	Graph *temp = copyGraph(graph);  
  	int loopLimit = (whichSCC ? temp->SCC[whichSCC]->states - 1 : temp->SCC[whichSCC]->states);

	for (i = 0; i  < loopLimit; i++) {  
		for (j = 0; j < temp->inputs; j++) {

			int state = list[(j * temp->states) + i] % temp->states;

			int input = temp->data[list[(j * temp->states) + i]]; 

			temp->data[list[(j * temp->states) + i]] = temp->states;

			if (DFS(temp, state, input)) 
			{
				iEs[k++] = list[(j * temp->states) + i];
			}
			else 
			{
				temp->data[list[(j * temp->states) + i]] = input;
			}

			if (limit && k >= limit) 
			{	
				break;
			}
		}
		if (limit && k >= limit) 
		{
			break;
		}
	}
	destroyGraph(temp); free(temp); temp = 0;
	*x = k;

	for (; k < graph->states * graph->inputs; k++) 
	{
		iEs[k] = -1;
	}
	return iEs;
}

/* Arrange the N elements of ARRAY in random order.
   Only effective if N is much smaller than RAND_MAX;
   if this may not be the case, use a better random
   number generator. */

void shuffle(int *array, size_t n)
{
    if (n > 1) 
    {
        size_t i;
        for (i = 0; i < n - 1; i++) 
        {
          size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
          int t = array[j];
          array[j] = array[i];
          array[i] = t;
        }
    }
}


/********************************************************
*	This function finds the unnecessary edges from 		*
*	SCC's to eliminate without canceling its SCC 		*
*	property and connects those unnecessary edges 		*
*	with other SCC's respect to args.					*
*********************************************************/
void ControlledSCCconnector(Graph **gArr, char*args[], int argc, Graph* last)
{	
	int * UnNeeded;
  	int i, k, m, j, processed = 0;
  	Graph * graph;
  	Graph * graph2;

	/*for (k = 0; k < graph->SCCs; k++)*/ 
	for(k = 0; k < atoi(args[4]) ; k++) {
		graph = gArr[k];
		for (i = 0; i < graph->inputs; i++)
		{
			for(j = processed; j< processed + graph->states; j++)
			{
				last->data[i*(last->states)+j] = graph->data[i*(graph->states)+j-processed] + processed;
			}
		}
		processed += graph->states;
	}
	int *shuffledList = (int *) malloc(sizeof(int) * (graph->states * graph->inputs));
	for(i = 0; i < (graph->states * graph->inputs); i++)
  	{
  		shuffledList[i] = i;
  	}
  	shuffle(shuffledList, (graph->states * graph->inputs));
	int count = 0;
	for(k = 5 + atoi(args[4]); k < argc; k += 3) {
		count++;
		graph = gArr[atoi(args[k])];
		processed = 0;
		for(j = 5; j < 5+atoi(args[k]); j++)
		{
			processed += atoi(args[j]);
		}
		
		//printf("%d", processed);
		
    	//printf("Limit: %d\n", graph->SCC[k]->states * graph->inputs);
		int numOfUnnEdges = 0;
		UnNeeded = getIEsNew(graph, 0, graph->states, &numOfUnnEdges, shuffledList);
		/*
		for(j=0; j<graph->states*graph->inputs; j++)
		{
			printf("%d, ", UnNeeded[j]);
		}
		printf("\n");
		*/
		for(i = 0; i < numOfUnnEdges*atof(args[k+1]); i++)
		{	
			int rand_edge = (rand() % numOfUnnEdges);
			while(last->data[last->states*(UnNeeded[rand_edge]/graph->states) + processed + UnNeeded[rand_edge]%graph->states] == -1 ||
				last->data[last->states*(UnNeeded[rand_edge]/graph->states) + processed + UnNeeded[rand_edge]%graph->states] >= processed + graph->states ||
				last->data[last->states*(UnNeeded[rand_edge]/graph->states) + processed + UnNeeded[rand_edge]%graph->states] < processed )
			{

				rand_edge = (rand() % numOfUnnEdges);
			}
			//printf("%d, ", UnNeeded[rand_edge]);
			last->data[last->states*(UnNeeded[rand_edge]/graph->states) + processed + UnNeeded[rand_edge]%graph->states] = -1;
		}

		/*
		printf("Automata ----------------------\n");
 		int i, j;
		for (i = 0; i < graph->inputs; ++i) 
		{
			printf("letter %c:\t", (char)(i + 97));
			for (j = 0; j < last->states; ++j) 
			{
				printf("%d\t" , last->data[i * last->states + j]);
			}
		printf("\n");
		}*/

		graph2 = gArr[atoi(args[k+2])];
		int processed2 = 0;
		for(j = 5; j < 5+atoi(args[k+2]); j++)
		{
			processed2 += atoi(args[j]);
		}

		//printf("Iteration%d and processed2 is %d\n: ", count, processed2);
		
		for(j = 0; j < graph->states; j ++)
		{
			for(i = 0; i < last->inputs; i++)
			{
				if (last->data[i*last->states + processed + j] == -1)
				{	
					int rand_edge = (rand() % graph2->states);
					last->data[i*last->states + processed + j] = processed2 + rand_edge;
				}
			}	
		}		
		//printf("\n");
	}
}

/*************************************************************
*  Connects the given two graphs by adding second graph      *
* to the end of first graph in a non-strongly connected way. *
**************************************************************/
Graph *connectGraph(Graph *graph1, Graph *graph2) {
	if (graph1->inputs != graph2->inputs) return 0;
	int i, j, k, *nOfConn = 0; int *iEs = 0, *marked = 0;
	Graph *graph = generateGraph(graph1->states + graph2->states, graph1->inputs), *temp = 0;
	for (i = 0; i < graph->states * graph->inputs; i++)
		if ((i % graph->states) < graph1->states)
			graph->data[i] = graph1->data[((i / graph->states) * graph1->states) + (i % graph->states)];
		else
			graph->data[i] = graph2->data[((i / graph->states) * graph2->states) + ((i % graph->states) - graph1->states)] + graph1->states;
	SCC(graph); nOfConn = (int *) malloc(sizeof(int) * (graph->SCCs - 1));
	if (graph->SCCs == 1) return graph; j = graph->SCCs;
	do {
		if (temp) { destroyGraph(temp); free(temp); }
		temp = copyGraph(graph);
		for (k = 0; k < graph->SCCs - 1; k++) {
			iEs = getIEs(graph, k + 1, 0); //We should find an appropriate limit to speed up the computation
			for (i = 0; i < (graph->SCC[k + 1]->states - 1) * graph->SCC[k + 1]->inputs; i++) if (iEs[i] == -1) break;
			nOfConn[k] = (rand() % (i + 1)); free(iEs);
		}
		while (!isAllSame(nOfConn, graph->SCCs - 1, 0)) {
			int base, target, input;
			do { base = (temp->SCCs > 1 ? (rand() % (temp->SCCs - 1)) + 1: temp->SCCs); } while(!temp->SCC[base] || !nOfConn[base - 1]);
			free(marked); marked = (int *) malloc(sizeof(int) * temp->SCC[base]->states * temp->inputs);
			iEs = getIEs(temp, base, nOfConn[base - 1]); for (i = 0; i < temp->SCC[base]->states * temp->inputs; i++) marked[i] = 0;
			while(nOfConn[base - 1]) {
				do {
					target = rand() % temp->SCCs;
					input = rand() % temp->states;
				} while(!temp->SCC[target] || target >= base || binarySearch(temp->SCC[target]->index, 0, temp->SCC[target]->states - 2, input) == -1);
				do { k = rand() % ((temp->SCC[base]->states - 1) * temp->inputs); } while(iEs[k] == -1 || marked[k]);
				temp->data[iEs[k]] = input; marked[k] = 1; nOfConn[base - 1]--;
			}
			free(iEs);
		}
		SCC(temp);
	} while(temp->SCCs != graph->SCCs);
	assignGraph(graph, temp); destroyGraph(temp); free(temp); free(nOfConn); free(marked);
	return graph;
}

/*******************************************************************
* Connects multiple Graphs by making use of connectGraph function. *
********************************************************************/
Graph *connectGraphs(Graph **gArr, int size) {
	if (size == 1) return copyGraph(gArr[0]);
	int i;
	Graph *graph = connectGraph(gArr[0], gArr[1]), *temp = 0;
	for (i = 2; i < size; i++) {
		temp = connectGraph(graph, gArr[i]);
		destroyGraph(graph); free(graph); graph = temp;
	}
	return graph;
}

/*********************************************************************************
* Joins SCCs of a given Graph and returns a strongly connected as a whole Graph. *
*********************************************************************************/
Graph *joinSCCs(Graph *mGraph) {
	int i, j, k, size; int *iEs = 0, *marked = (int *) malloc(sizeof(int) * mGraph->states * mGraph->inputs), *nOfConn = 0; 
	Graph *graph = copyGraph(mGraph);
	SCC(graph); if (graph->SCCs == 1) return graph;
	do {
		nOfConn = (int *) malloc(sizeof(int) * graph->SCCs);
		for (k = 0; k < graph->SCCs; k++) {
			iEs = getIEs(graph, k, 0); //We should find an appropriate limit to speed up the computation
			for (i = 0; i < (k ? (graph->SCC[k]->states - 1) * graph->inputs : graph->SCC[k]->states * graph->inputs) && iEs[i] != -1; i++);
			nOfConn[k] = (i > 1 ? (rand() % (i - 1)) + 1 : i); free(iEs);
		}
		while (!isAllSame(nOfConn, graph->SCCs, 0)) {
			int base = rand() % graph->SCCs, target, input;
			while(!graph->SCC[base] || !nOfConn[base]) base = (base + 1) % graph->SCCs;
			int size = (!base ? graph->SCC[base]->states * graph->inputs : (graph->SCC[base]->states - 1) * graph->inputs);
			iEs = getIEs(graph, base, nOfConn[base]); memset(marked, 0, sizeof(int) * size);
			while(nOfConn[base]) {
				target = rand() % graph->SCCs;
				while(!graph->SCC[target] || target == base) target = (target + 1) % graph->SCCs;
				input = rand() % graph->states;
				while(binarySearch(graph->SCC[target]->index, 0, (target ? graph->SCC[target]->states - 2 : graph->SCC[target]->states - 1), input) == -1) input = (input + 1) % graph->states;
				k = rand() % size; int counter = 1;
				while((iEs[k] == -1 || marked[k]) && counter != size) { k = (k + 1) % size; counter++; }
				if (counter == size) { nOfConn[base] = 0; break; }
				graph->data[iEs[k]] = input; marked[k] = 1; nOfConn[base]--;
			}
			free(iEs);
		}
		SCC(graph); free(nOfConn);
	} while(graph->SCCs != 1); free(marked);
	return graph;
}

void saveGraph(Graph *graph, FILE *file) {
	if (!file) return;
	int i, j;
	fprintf(file, "%d %d \n", graph->states, graph->inputs);
	for (i = 0; i < graph->states * graph->inputs; i++) {
		fprintf(file, "%d ", graph->data[i]);
		if (i % graph->states == graph->states - 1)
			fprintf(file, "\n");
	}
}

Graph *loadGraph(FILE *file) {
	if (!file) {printf("loadGraph could not load graph\n %d", 1 ); return 0;}
	int i = -1, j = -1, ch, negative = 1, input = -1, states = -1, inputs = -1;
	DynIntStack *stack = generateDynIntStack(); Graph *graph = 0;
	while((ch = fgetc(file)) != EOF) {
		if (ch >= '0' && ch <= '9')
			stackPush(stack, ch - '0');
		else if (ch == ' ') {
				if (states == -1) states = toInteger(stack) * negative;
				else if (inputs == -1) inputs = toInteger(stack) * negative;
				else input = toInteger(stack) * negative;
				j++; negative = 1;
		} else if (ch == '\n') { i++; j = -1; input = -1; } else if (ch == '-') negative = -1;
		if (i != -1 && (i >= inputs || j >= states)) break;
		if (states != -1 && inputs != -1 && i == -1) graph = generateGraph(states, inputs);
		else if (input != -1) graph->data[(i * states) + j] = input;
	} destroyDynIntStack(stack); free(stack);

	

	if (graph) SCC(graph);
	return graph;
	
}

/*********************************
* Returns ?th SCC a state is in  *
**********************************/
int inSCC(int state, Graph *graph){
	int i = 0, j;
	while(i <= graph->SCCs && graph->SCC[i]) {
		j = binarySearch(graph->SCC[i]->index, 0, graph->SCC[i]->states - (i ? 2 : 1), state);
		if (j != -1) return i; i++;
	}
	return -1;
}

Graph **processSCCs(Graph *graph) {
	Graph **SCC = (Graph **) malloc(sizeof(Graph *) * graph->states), **gArr = SCC;
	int i, j, k, *indices = (int *) malloc(sizeof(int) * graph->SCCs), *temp = indices; for (i = 0; i < graph->SCCs; i++) indices[i] = -1;
	DynIntStack *stack = generateDynIntStack();
	for (i = graph->SCCs - 1; i > 0; i--) {
		int flag = 1;
		for (j = graph->SCCs - 1; j > i; j--) {
			int isExists = 0; for (k = 0; k < graph->SCCs && !isExists; k++) if (indices[k] == j) isExists = 1;
			if (!isExists && !isAllSame(graph->SCCrel[j][i], graph->inputs, 0)) flag = 0;
		}
		if (flag) { *temp++ = i; stackPush(stack, i); }
		else {
			DynIntStack *revStack = generateDynIntStack(); int stateSum = 0;
			while(!stackEmpty(stack)) { stateSum += graph->SCC[stackTop(stack)]->states -  1; stackPush(revStack, stackTop(stack)); stackPop(stack); }
			Graph *newGraph = generateGraph(stateSum + 1, graph->inputs);
			newGraph->index = (int *) malloc(sizeof(int) * stateSum); int lastK = 0;
			while(!stackEmpty(revStack)) {
				int pos = stackTop(revStack); stackPop(revStack);
				Graph *curGraph = graph->SCC[pos]; 
				for (k = 0; k < curGraph->states - 1; k++) {
					newGraph->index[lastK++] = curGraph->index[k];
				}
			} bubbleSort(newGraph->index, stateSum); int l;
			for (l = 0; l < newGraph->states; l++)
				for (k = 0; k < newGraph->inputs; k++)
					if (l == newGraph->states - 1) { 
						newGraph->data[(k * newGraph->states) + l] = l; 
					} else {
						int s = graph->data[(k * graph->states) + newGraph->index[l]];
						s = binarySearch(newGraph->index, 0, newGraph->states - 2, s);
						if (i == -1)
							newGraph->data[(k * newGraph->states) + l] = newGraph->states - 1;
						else
							newGraph->data[(k * newGraph->states) + l] = s;
					}
			*gArr++ = newGraph; destroyDynIntStack(revStack); free(revStack);
		}
	}
	if (stack->size == graph->SCCs - 1) { printf("regular\n"); for (i = 0; i < graph->SCCs; i++) SCC[i] = copyGraph(graph->SCC[i]); }
	free(indices); destroyDynIntStack(stack); free(stack);
	return SCC;
}


void DFSUtil(Graph *graph, int u, int v, int *visited) {
	int i;
	visited[u] = 1;

	for (i = 0; i < graph->inputs && !visited[v]; i++) 
	{
		int w = graph->data[(i * graph->states) + u];

		if (w < 0 || w >= graph->states) 
		{
			continue;
		}

		if (!visited[w]) 
		{
			DFSUtil(graph, w, v, visited);
		}
	}
}

int DFS(Graph *graph, int u, int v) {
	int i;

	int *visited = (int *) malloc(sizeof(int) * graph->states);
	for (i = 0; i < graph->states; i++) 
	{
		visited[i] = 0;
	}

	DFSUtil(graph, u, v, visited);
	
	i = visited[v];
	free(visited);

	return i;
}

/******************************************************************
*  Recursive function to implement Tarjan's algorithm,            *
* adds SCCs to the given graph, in a topologically sorted manner. *
*******************************************************************/
void SCCUtil(Graph *graph, int u, int *disc, int *low, DynIntStack *stack, int *stackMember, int *time) {
	int i, j, k, w;
	disc[u] = low[u] = ++*time;
	stackPush(stack, u);
	stackMember[u] = 1;

	for (j = 0; j < graph->inputs; j++) {
		int v = graph->data[(j * graph->states) + u]; if (v < 0 || v >= graph->states)  continue;

		if (disc[v] == -1) {
			SCCUtil(graph, v, disc, low, stack, stackMember, time);
			low[u] = low[u] < low[v] ? low[u] : low[v];
		} else if (stackMember[v]) {
			low[u] = low[u] < disc[v] ? low[u] : disc[v];
		}
	}

	DynIntStack *temp = generateDynIntStack();
	if (low[u] == disc[u]) {
		while (!stackEmpty(stack) && stackTop(stack) != u) {
			w = stackTop(stack);
			stackPush(temp, w);
			stackMember[w] = 0;
			stackPop(stack);
		}
		if (stackEmpty(stack)) return;
		w = stackTop(stack);
		stackMember[w] = 0;
		stackPop(stack);
		stackPush(temp, w); 
		Graph **gArr = graph->SCC;
		while (*gArr) gArr++;
		if (temp->size == graph->states) 
			*gArr = graph;
		else {
			*gArr = generateGraph(!graph->SCCs ? temp->size : temp->size + 1, graph->inputs); 
			(*gArr)->SCCs = 1; (*gArr)->SCC[0] = (*gArr);
		}
			
		(*gArr)->index = (int *) malloc(sizeof(int) * temp->size);
		j = 0;
		while(!stackEmpty(temp)) {
			w = stackTop(temp);
			stackPop(temp);
			(*gArr)->index[j++] = w;
		}
		bubbleSort((*gArr)->index, (!graph->SCCs ? (*gArr)->states : (*gArr)->states - 1)); //Can be improved!

		if (*gArr != graph)	{
			for (j = 0; j < (*gArr)->states; j++)
				for (k = 0; k < (*gArr)->inputs; k++)
					if (graph->SCCs && j == (*gArr)->states - 1) { 
						(*gArr)->data[(k * (*gArr)->states) + j] = j; 
					} else {
						int s = graph->data[(k * graph->states) + (*gArr)->index[j]];
						i = binarySearch((*gArr)->index, 0, (!graph->SCCs ? (*gArr)->states - 1 : (*gArr)->states - 2), s);
						if (i == -1) {
							(*gArr)->data[(k * (*gArr)->states) + j] = (*gArr)->states - 1;
						}
						else
							(*gArr)->data[(k * (*gArr)->states) + j] = i;
					}
		}
		graph->SCCs++;
	}
	destroyDynIntStack(temp); free(temp); 
}

/*******************************************************************************
* The function to find and add SCCs of the given graph, making use of SCCUtil. *
********************************************************************************/
void SCC(Graph *graph) {
	int i, j, k, *time = (int *) malloc(sizeof(int));
	int *disc = (int *) malloc(sizeof(int) * graph->states);
	int *low = (int *) malloc(sizeof(int) * graph->states);
	int *stackMember = (int *) malloc(sizeof(int) * graph->states);
	DynIntStack *stack = generateDynIntStack();

	*time = 0;
	for (i = 0; i < graph->states; i++) {
		disc[i] = low[i] = -1;
		stackMember[i] = 0;
		if (graph->SCC[i] && graph != graph->SCC[i]) { destroyGraph(graph->SCC[i]); free(graph->SCC[i]); } 
		else if (graph->SCC[i]) graph->SCC[i] = 0;
		graph->SCC[i] = 0;
	}
	graph->SCCs = 0;
	free(graph->stateSCCs); free(graph->index);
	graph->stateSCCs = 0; graph->index = 0;
	if (graph->SCCrel) {
		for (i = 0; i < graph->SCCs; i++) {
			for (j = 0; j < graph->SCCs; j++)
				free(graph->SCCrel[i][j]);
			free(graph->SCCrel[i]);
		}
	}
	free(graph->SCCrel); graph->SCCrel = 0;

	for (i = 0; i < graph->states; i++)
		if (disc[i] == -1)
			SCCUtil(graph, i, disc, low, stack, stackMember, time);
	
	//loop to fill stateSCCs
	graph->stateSCCs = (int *) malloc(sizeof(int) * graph->states);
	if (graph->SCCs == 1) for (i = 0; i < graph->states; i++) graph->stateSCCs[i] = 0;
	else for(i = 0; i < graph->states; i++) graph->stateSCCs[i] = inSCC(i, graph);
	
	// loop that fills SCCrel data. 
	// each graph->SCCrel[from][to] is an array with the size of inputs
	// array holds the amount of edges from "from"th SCC to "to"th SCC
	graph->SCCrel = (int ***) malloc(sizeof(int **) * graph->SCCs);
	for(i = 0; i < graph->SCCs; i++){
		graph->SCCrel[i] = (int **) malloc(sizeof(int *) * graph->SCCs);
		for(j = 0; j < graph->SCCs; j++)
		{
			graph->SCCrel[i][j] = (int *) malloc(sizeof(int) * graph->inputs);
			for (k = 0; k < graph->inputs; k++) 
				if (graph->SCCs == 1) graph->SCCrel[i][j][k] = graph->states;
				else graph->SCCrel[i][j][k] = 0;
		}
		if (graph->SCCs > 1)
			for (j = 0; j < (i ? graph->SCC[i]->states - 1 : graph->SCC[i]->states); j++)
					for (k = 0; k < graph->inputs; k++){
						int toSCC = inSCC(graph->data[(k * graph->states) + graph->SCC[i]->index[j]], graph);
						if (toSCC != -1) graph->SCCrel[i][toSCC][k]++;
	  				}
	}
	destroyDynIntStack(stack); free(stack);
	free(time); free(disc); free(low); free(stackMember);
}