#include <iostream>
#include <fstream>
#include <cmath>
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
//#include "omp.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <queue> 
#include "naive.h"


using namespace std;

int checkInverse(int *a, int* iap, int* ia, int N, int P) {
	for (int p = 0; p < P; p++) {
		for (int i = 0; i < N; i++) {
			int target = a[p * N + i];

			int found = 0;
			for (int iaptr = iap[p * (N + 1) + target]; iaptr < iap[p * (N + 1) + target + 1]; ++iaptr) {
				int incoming = ia[p * N + iaptr];
				
				if (i == incoming) {
					found = 1;
					break;
				}
			}

			if (!found) {
				cout << "something is wrong " << i << " goes to " << target << " with " << p << " but it is not in the inverse automata\n";
				exit(1);
			}
		}
	}

	for (int p = 0; p < P; p++) {
		for (int i = 0; i < N; i++) {
			for (int iaptr = iap[p * (N + 1) + i]; iaptr < iap[p * (N + 1) + i + 1]; ++iaptr) {
				int source = ia[p * N + iaptr];
				if (a[p * N + source] != i) {
					cout << "something is wrong " << i << " has " << source << " in inverse automata but it " << source << " goes to " << a[p * N + source] << " with " << p << "\n";
					exit(1);
				}
			}
		}
	}

	return 0;
}

int main(int argc, char *argv[]) {
	
	int level = 1;
	
	if (argc != 3) {
		cout << "Usage: " << argv[0] << " input_filename output_filename\n" << endl;
		return 1;
	}

	FILE *input_file = fopen(argv[1], "r"); 
	Graph *lastGraph = 0, *graph = 0;
	while((graph = loadGraph(input_file))) {
		if (lastGraph) { destroyGraph(lastGraph); free(lastGraph); }
		lastGraph = graph;
	} graph = lastGraph; //last graph in the file: connected graph
	printf("Graph with %d states, %d inputs and %d SCC(s)\n", graph->states, graph->inputs, graph->SCCs);
	char *scc_name = (char *) malloc(sizeof(char) * (strlen(argv[2]) + 13));
	sprintf(scc_name, "scc_synchroP_%s", argv[2]);
	char *greedy_name = (char *) malloc(sizeof(char) * (strlen(argv[2]) + 9));
	sprintf(greedy_name, "synchroP_%s", argv[2]);
	FILE *output_scc_file = fopen(scc_name, "w"), *output_greedy_file = fopen(greedy_name, "w"); 
	saveGraph(graph, output_scc_file); fprintf(output_scc_file, "---------\nRESULTS\n---------\n");
	saveGraph(graph, output_greedy_file); fprintf(output_greedy_file, "---------\nRESULTS\n---------\n");

#ifdef LOG_LEVEL
	struct stat st = {0};
	if (stat("results_greedy", &st) == -1) {
		mkdir("results_greedy", 0700);
	}
	char* filename = new char[256];
#if LOG_LEVEL == TIME_ANALYSIS
	sprintf(filename, "results_greedy/timeAndLength_%s_%s.csv", "10", "2");
#elif LOG_LEVEL == DATA_ANALYSIS
	sprintf(filename, "results_greedy/data_%s_%s.csv", "10", "2");
#endif
	if (strcmp("1", "HEADER") == 0) {
		out.open(filename);
		if (level & 1) {
#if LOG_LEVEL == TIME_ANALYSIS
			out << "bfs_time_naive,";
			out << "time_naive,";
			out << "length_naive";
#elif LOG_LEVEL == DATA_ANALYSIS
			out << "allnodes_naive,";
			out << "nodes_naive,";
			out << "level_naive,";
			out << "maxlevel_naive,";
			out << "meanlevel_naive";
#endif
			if ((level ^ 1) > 1)
				out << ",";
		}
		
		out << endl;
		out.close();
		return 0;
	}
	out.open(filename, ios::app);
	free(filename);
#endif

	if (level & 1) {
		long long start = current_timestamp();
		PNode* globpath = NULL;	
		PNode* ptr1 = globpath;

		cout << endl << endl << "SCC METHOD" << endl << endl;

		int *glob_actives = new int[graph->states], glob_no_actives = graph->states;
		for (int i = 0; i < graph->states; i++)
			glob_actives[i] = i;

		PNode* path = NULL;
		Graph **SCC = processSCCs(graph);

		for(int i = graph->SCCs - 1; i >= 0; i--) { 
			if (!SCC[i]) continue;
			int N = SCC[i]->states, P = SCC[i]->inputs;
			int * automata = SCC[i]->data;

#ifdef DEBUG		
			printAutomata(automata, N, P);
#endif
			int* inv_automata_ptrs = new int[P * (N + 1)];					
			int* inv_automata = new int[P * N];

			//#pragma omp parallel for schedule(static)
			for (int i = 0; i < P; ++i) {
				int *a = &(automata[i * N]);
				int* ia = &(inv_automata[i * N]);
				int* iap = &(inv_automata_ptrs[i * (N + 1)]);

				memset(iap, 0, sizeof(int) * (N + 1));
				for (int j = 0; j < N; j++) {iap[a[j] + 1]++;}
				for (int j = 1; j <= N; j++) {iap[j] += iap[j - 1];}
				for (int j = 0; j < N; j++) {ia[iap[a[j]]++] = j;}
				for (int j = N; j > 0; j--) {iap[j] = iap[j - 1];} iap[0] = 0;
			}

			applyPath(graph->data, path, graph->states, glob_actives, glob_no_actives);
			
			int no = 0;
			for (int j = 0; j < glob_no_actives; j++)
				if (binarySearch(SCC[i]->index, 0, i ? N - 2 : N - 1, glob_actives[j]) != -1)
					no++;

			int* actives = new int[no]; int k = 0;
			for (int j = 0; j < glob_no_actives && k < no; j++) {
				int index = binarySearch(SCC[i]->index, 0, i ? N - 2 : N - 1, glob_actives[j]);
				if (index != -1)
					actives[k++] = index;
			}

			int* active_marker = new int[N];
			for (int i = 0; i < N; i++) {
				active_marker[i] = 0;
			}

			DynIntStack *stack = generateDynIntStack();

			for (int i = 0; i < no; i++) {
				int act = actives[i];
				if (active_marker[act] != 1) {
					active_marker[act] = 1;
					stackPush(stack, act);
				}
			} 
			if (i) stackPush(stack, (SCC[i]->states - 1)); 

			delete[] actives;

			int no_actives = stack->size;
			actives = new int[no_actives];
			for (int i = 0; i < no_actives; i++) {
				actives[i] = stackTop(stack);
				stackPop(stack);
			}

			destroyDynIntStack(stack);

			delete[] active_marker;

			checkInverse(automata, inv_automata_ptrs, inv_automata, N, P);
			
#ifdef DEBUG	
			printInverseAutomata(inv_automata_ptrs, inv_automata, N, P);			
#endif			
			delete path; path = NULL;
			greedyHeuristic_naive(automata, inv_automata_ptrs, inv_automata, N, P, path, globpath, actives, no_actives);
			cout << "Path is found: "; 
			PNode* pnode = path;
			int plength = 0;
			while (pnode) {
				cout << pnode->letter << " "; 
				pnode = pnode->next;
				plength++;
			}
			if (i == 0) fprintf(output_scc_file, "PATH: ");
			cout << endl << "Path length is " << plength;
			cout << endl << "After this turn globalpath is : ";
			PNode* pnode2 = globpath;
			int gplength = 0;
			while (pnode2) {
				cout << pnode2->letter << " "; if (i == 0) fprintf(output_scc_file, "%d ", pnode2->letter);
				pnode2 = pnode2->next;
				gplength++;
			}
			if (i == 0) fprintf(output_scc_file, "\nLENGTH: %d\n", gplength);
			cout << endl << "Global Path length is " << gplength << endl;
			//cout << "Number of remaining actives for " << i << "th SCC is " << applyPath(automata, path, N, actives) << endl << endl;

			/*int * lastState = (int *) malloc(sizeof(int) * N);
			for (int i = 0; i < N; i++) lastState[i] =  i;
			PNode* ptr = globpath;
			while(ptr) {
				cout << ptr->letter << ": ";
				for (int i = 0; i < N; i++) {
					lastState[i] = automata[(ptr->letter * N) + lastState[i]];
					cout << lastState[i] << " ";
				}
				cout << endl;
				ptr = ptr->next;
			} free(lastState);*/

			delete[] actives;
	#if LOG_LEVEL == TIME_ANALYSIS
			out << plength;
	#endif
			delete[] inv_automata; delete[] inv_automata_ptrs;
		} long long finish = current_timestamp(); 
		delete[] glob_actives; fprintf(output_scc_file, "TIME: %lld ms\n", finish - start);
		fprintf(output_scc_file, "SCCs: "); for(int i = graph->SCCs - 1; i >= 0; i--) if (!SCC[i]) continue; else fprintf(output_scc_file, "%d ", SCC[i]->states - (i ? 1 : 0));

		fprintf(output_scc_file, "\n---------\nCHECK\n---------\n");
		int * lastState = (int *) malloc(sizeof(int) * graph->states);
		for (int i = 0; i < graph->states; i++) lastState[i] =  i;
		PNode* ptr = globpath;
		while(ptr) {
			fprintf(output_scc_file, "%d: ", ptr->letter);
			for (int i = 0; i < graph->states; i++) {
				lastState[i] = graph->data[(ptr->letter * graph->states) + lastState[i]];
				fprintf(output_scc_file, "%d ", lastState[i]);
			}
			fprintf(output_scc_file, "\n");
			ptr = ptr->next;
		}
		fprintf(output_scc_file, "---------\nEND\n---------\n"); fclose(output_scc_file);
	#ifdef LOG_LEVEL
			if ((level ^ 1) > 1)
				out << ",";
	#endif

		start = current_timestamp();

		int N = graph->states, P = graph->inputs;
		int * automata = graph->data;

#ifdef DEBUG		
		printAutomata(automata, N, P);
#endif
		int* inv_automata_ptrs = new int[P * (N + 1)];					
		int* inv_automata = new int[P * N];

		//#pragma omp parallel for schedule(static)
		for (int i = 0; i < P; ++i) {
			int *a = &(automata[i * N]);
			int* ia = &(inv_automata[i * N]);
			int* iap = &(inv_automata_ptrs[i * (N + 1)]);

			memset(iap, 0, sizeof(int) * (N + 1));
			for (int j = 0; j < N; j++) {iap[a[j] + 1]++;}
			for (int j = 1; j <= N; j++) {iap[j] += iap[j - 1];}
			for (int j = 0; j < N; j++) {ia[iap[a[j]]++] = j;}
			for (int j = N; j > 0; j--) {iap[j] = iap[j - 1];} iap[0] = 0;
		}
			
		int* actives = new int[N];
		for (int i = 0; i < N; ++i) {
			actives[i] = i;
		}

		int no_actives = N;

		checkInverse(automata, inv_automata_ptrs, inv_automata, N, P);

#ifdef DEBUG	
		printInverseAutomata(inv_automata_ptrs, inv_automata, N, P);
#endif

		cout << endl << endl << "SYNCHROP METHOD" << endl << endl;

		delete path; delete globpath;

		path = NULL; globpath = NULL;
		greedyHeuristic_naive(automata, inv_automata_ptrs, inv_automata, N, P, path, globpath, actives, no_actives);
		cout << "Path is found: "; 
		fprintf(output_greedy_file, "PATH: ");
		PNode* pnode = path;
		int plength = 0;
		while (pnode) {
			cout << pnode->letter << " "; fprintf(output_greedy_file, "%d ", pnode->letter);
			pnode = pnode->next;
			plength++;
		}
		cout << endl << "Path length is " << plength << endl;
		fprintf(output_greedy_file, "\nLENGTH: %d\n", plength);
		applyPath(automata, path, N, actives, no_actives);
		cout << "Number of remaining actives is " << no_actives << endl << endl;
		delete[] actives;

		finish = current_timestamp();
		fprintf(output_greedy_file, "TIME: %lld ms\n", finish - start);
		fprintf(output_greedy_file, "SCCs: "); for(int i = graph->SCCs - 1; i >= 0; i--) fprintf(output_greedy_file, "%d ", graph->SCC[i]->states - (i ? 1 : 0));

		fprintf(output_greedy_file, "\n---------\nCHECK\n---------\n");
		for (int i = 0; i < graph->states; i++) lastState[i] =  i;
		ptr = path;
		while(ptr) {
			fprintf(output_greedy_file, "%d: ", ptr->letter);
			for (int i = 0; i < graph->states; i++) {
				lastState[i] = graph->data[(ptr->letter * graph->states) + lastState[i]];
				fprintf(output_greedy_file, "%d ", lastState[i]);
			}
			fprintf(output_greedy_file, "\n");
			ptr = ptr->next;
		}
		fprintf(output_greedy_file, "---------\nEND\n---------\n"); fclose(output_greedy_file);
		free(lastState);

		}
	#ifdef LOG_LEVEL
		out << endl;
		out.close();
	#endif
	destroyGraph(graph); free(graph);
	printf("Check the files: scc_method: %s synchroP_method: %s\n", scc_name, greedy_name);
	free(scc_name);
	free(greedy_name);
	return 0;
}

