#!/usr/bin/python
import os
import sys
import re
import random
from math import floor


class Node(object):
    def __init__(self, data):
        self.data = data
        self.children = []

    def add_child(self, obj):
        self.children.append(obj)

def randomSCC(numSCC):
    connectToSCC = random.randint(0,numSCC)
    return connectToSCC




#SCC start from 0 to len-1
def creator():  # filename, seed(?), input count, scc count, scc state sizes, methodnum
    stateSize1 = [64, 128, 256]
    sccNum1 = [4, 8, 16]
    sccNum2 = [3, 7, 15]
    inputSize = [2, 4, 8]
    distFactor = [0.25, 0.5, 0.75]
    experimentSize = 51

    #testresults = open("new_graphs_testresults_greedy2.csv","w")
    #testresults.write("sep=,\n")
    #testresults.write("Total States,States,Inputs,SCC,Seed,SCC Length,SCC Time(ms),Algo Length,Algo Time(ms)\n")

    # lineer connection
    for s in stateSize1:
        for p in inputSize:
            for c in sccNum1:
                for r in distFactor:
                    for expID in range(1, experimentSize):
                        # linear connection
                        connections = ""
                        # c-1 because we wont do anything with first scc
                        for i_1 in range(c-1):
                            if i_1 == 0:
                                connections = str(
                                    c - (i_1+1)) + " " + str(r) + " " + str(c - (i_1+2))
                            else:
                                connections = connections + " " + \
                                    str(c - (i_1+1)) + " " + \
                                    str(r) + " " + str(c - (i_1+2))
                        # first number is for showing which method of connection is using.
                        filename = "0" + "_" + \
                            str(s) + "_" + str(p) + "_" + str(c) + "_" + \
                            str(int(r*100)) + "_" + str(expID*2) + ".txt"
                        cmd = "./generator " + filename + " " + \
                            str(expID*2) + " " + str(p) + " " + \
                            str(c) + " " + (str(s) + " ") *c + " " + connections
                        print(cmd)
                        os.system(cmd + " > /dev/null")
                        # connect scc to previous
                        connections2 = ""
                        # because it starts from 0 and we want it to be connected in a backwards manner
                        for i_2 in range(c-1):
                            fraction = r/(c-i_2-1)
                            for k in range(c-(i_2+1)):
                                if i_2 == 0 and k == 0:
                                    connections2 = str(c-i_2-1) + " " + str(fraction) + " " + str(k)
                                else:
                                    connections2 = connections2 + " " + \
                                        str(c-i_2-1) + " " + str(fraction) + " " + str(k)
                        # first number is for showing which method of connection is using.
                        filename = "1" + "_" + \
                            str(s) + "_" + str(p) + "_" + str(c) + "_" + \
                            str(int(r*100)) + "_" + str(expID*2) + ".txt"
                        cmd = "./generator " + filename + " " + \
                            str(expID*2) + " " + str(p) + " " + \
                            str(c) + " " + (str(s)+ " ") *c + " " + connections2
                        print(cmd)
                        os.system(cmd + " > /dev/null")

                        # connect all scc to first
                        connections3 = ""
                        for i_3 in range(c-1):
                            if i_3 == 0:
                                connections3 = str(c-(i_3+1)) + " " + str(r) + " " + "0"
                            else:
                                connections3 = connections3 + " " + \
                                    str(c-(i_3+1)) + " " + str(r) + " " + "0"
                        # first number is for showing which method of connection is using.
                        filename = "2" + "_" + \
                            str(s) + "_" + str(p) + "_" + str(c) + "_" + \
                            str(int(r*100)) + "_" + str(expID*2) + ".txt"
                        cmd = "./generator " + filename + " " + \
                            str(expID*2) + " " + str(p) + " " + \
                            str(c) + " " + (str(s) + " ") *c + " " + connections3
                        print(cmd)
                        os.system(cmd + " > /dev/null")
                        #Random Connection
                        #Check if works
                        connections5 =""                        
                        for i_4 in range(c-1):
                            rand = randomSCC(c-(i_4+2))
                            if i_4 == 0:
                                connections5 = str(c-(i_4+1)) + " " + str(r) + " " + str(rand)
                            else:
                                connections5 = connections5 + " " + \
                                    str(c-(i_4+1)) + " " + str(r) + " " + str(rand)
                        filename = "5" + "_" + \
                            str(s) + "_" + str(p) + "_" + str(c) + "_" + \
                            str(int(r*100)) + "_" + str(expID*2) + ".txt"
                        cmd = "./generator " + filename + " " + \
                            str(expID*2) + " " + str(p) + " " + \
                            str(c) + " " + (str(s) + " ") *c + " " + connections5
                        print(cmd)
                        os.system(cmd + " > /dev/null")
    # tree connection
    for s in stateSize1:
        for p in inputSize:
            for c in sccNum2:  # scc num is different
                for r in distFactor:
                    for expID in range(1, experimentSize):
                        connections = ""
                        tree = Node(k+1)
                        for k in range(1,c):
                            tree.add_child(k+1)

                            i = c
                            while i > 1:
                                if i == c:
                                    connections4 = str(i) + " " + str(r) + " " + str(i/2) + \
                                        " " + str(i-1) + " " + \
                                        str(r) + " " + str(i/2)
                                else:
                                    connections4 = connections4 + " " + \
                                        str(i) + " " + str(r) + " " + str(i/2) + \
                                        " " + str(i-1) + " " + \
                                        str(r) + " " + str(i/2)
                                i = i-2
                                # first number is for showing which method of connection is using.
                        filename = "3" + "_" + \
                            str(s) + "_" + str(p) + "_" + str(c) + \
                            "_" + str(int(r*100)) + "_" + str(expID*2) + ".txt"
                        cmd = "./generator " + filename + " " + \
                            str(expID*2) + " " + str(p) + " " + \
                            str(c) + " " + (str(s) + " ")*c + " " + connections4
                        print(cmd)
                        os.system(cmd + " > /dev/null")

     #testresults.close()

def main():

    creator()


if __name__ == "__main__":
    main()